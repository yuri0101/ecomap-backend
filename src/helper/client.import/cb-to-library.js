const config = require("../../config/config")
const mongoose = require('mongoose')
const ObjectId = mongoose.Types.ObjectId;

const ClearBitModel = require("../../models/dw-clearbit.model")
const UserTeamStakeholder = require("../../models/user-team-stakeholder.model")

async function startMigration(){
    let data = await ClearBitModel.find({})
    // console.log(data)
    let utS = []

    for(let i=0; i<data.length; i++){
        
        let cb = data[i]
        let payload = {
            user_team:ObjectId('606043ddea572a2055ba0fb4'),
            name: cb.name || cb.legalName,
            details:'',
            image: cb.logo,
            s_info:{
                website: cb.domain, 
                country: cb.country || ''
            },
            info:{
                name: cb.name,
                image: cb.logo,
                website: cb.domain,
            },
            tags:[],
            metrics:{
                employees:{},
                revenue:{}
            },
            industries: cb.industries,
            _private:{
                data_source:{
                    clearbit: ObjectId(cb.id)
                }
            }
        }

        if(cb.metrics && cb.metrics.employees){
            payload.metrics.employees = {
                exact: cb.metrics.employees
            }
        }else{
            if(cb.metrics && cb.metrics.employeesRange){
                payload.metrics.employees = {
                    range: cb.metrics.employeesRange
                }
            }
        }

        if(cb.metrics && cb.metrics.annualRevenue){
            payload.metrics.revenue = {
                exact: cb.metrics.annualRevenue
            }
        }else{
            if(cb.metrics && cb.metrics.estimatedAnnualRevenue){
                payload.metrics.revenue = {
                    range: cb.metrics.estimatedAnnualRevenue
                }
            }
        }
        await UserTeamStakeholder.create(payload)
        .then(d=>{
            console.log(`Migrated for ${cb.id}`)
        })
        .catch(e=>{
            console.log(`Failed for Index: ${i} ${cb.id}`)
        });
        // utS.push(payload)
    }

    // console.log(utS)
    // let x = await UserTeamStakeholder.insertMany(utS);
    // console.log("Inserted:")
}

mongoose.connect(config.mongoose.url, config.mongoose.options).then(() => {
    startMigration().then(d=>{})
})
