const config = require("../config/config")
const data = require("./data")
const mongoose = require('mongoose')
const ObjectId = mongoose.Types.ObjectId;

const ClearBitModel = require("../models/dw-clearbit.model")
const IndustryModel = require("../models/industry.model")
let Clearbit = require("../vendor/clearbit/")
var i = 5

async function main(domain, country, industries){
   
    //console.log(x, industries)
    let y = await ClearBitModel.findOne({domain})
    if(!y){

        let x = await Clearbit.getInfo(domain)
        x.clearbit_id = x.id;
        delete x.id;
    
        await ClearBitModel.create({   
            ...x, 
            domain,
            country,
            industries
        }).then(d=>{
            let x = d ? d.id:"NONE:" + domain
            console.log(`Created with ID: ${x}`)
        }).catch(e=>{
            //console.log(x)
            if(e.code === 11000){
                console.log("Duplicate Key Error at: ", i, e.message)
            }else{
                console.error(`Error Creating ${i} Module for: ${domain}`, e)
                process.exit()
            }
        })
    }else{
        let x = await Clearbit.getInfo(domain)
        x.clearbit_id = x.id;
        delete x.id;
        console.log(x)
        console.log("Module Already There.")
    }
    
    return ''
}

// var i = data.length-1

//console.log(data[i]); process.exit;

mongoose.connect(config.mongoose.url, config.mongoose.options).then(() => {
    console.info('Connected to MongoDB');
    function updateData(){

        let m = data[i]

        IndustryModel.findOneOrCreate(
            {
                title:new RegExp(m['Landscape subcategory'], 'i')
            },
            {
                title: m['Landscape subcategory']
            },
            (err, subIndustry)=>{
                
                IndustryModel.findOneOrCreate(
                    {
                        title: new RegExp(m['Landscape category'], 'i')
                    },
                    {
                        title: m['Landscape category']
                    },
                    (err, mainIndustry)=>{

                        if(mainIndustry.child.length == 0){
                            mainIndustry.child = [...mainIndustry.child, subIndustry._id]
                            mainIndustry.save((err, result)=>{
                                if(err) console.log(`Error Saving Industry: ${err}`)
                                else console.log("Industry Updated")
                            })
                        }

                        let domain = m['URL'].toLowerCase()

                        console.log("Industries: ", mainIndustry._id, subIndustry._id)
                        main(
                            domain, 
                            m['Country'],
                            [ObjectId(mainIndustry._id), ObjectId(subIndustry._id)]
                        ).then(e=>{
                            // console.log("Module Added : ", e)
                        })

                    }
                )
                
            }
        )

        setTimeout(()=>{
            i--;
            if(i >= 0){
                updateData()
            }
        },500)

        //main(m.URL).then(e=>console.log(e))
    }
    updateData()
});

