module.exports = [
    {
      "Country": "Austria",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Mobile Marketing",
      "URL": "Mobfox.Com"
    },
    {
      "Country": "Austria",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Mobile Marketing",
      "URL": "Adatch.Com"
    },
    {
      "Country": "Austria",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Search & Social Advertising",
      "URL": "Smartengine.Solutions"
    },
    {
      "Country": "Austria",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Auphonic.Com"
    },
    {
      "Country": "Austria",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Remove.Bg"
    },
    {
      "Country": "Austria",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Lingohub.Com"
    },
    {
      "Country": "Austria",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Marmind.Com"
    },
    {
      "Country": "Austria",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Brandquiz.Io"
    },
    {
      "Country": "Austria",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Pinpoll.Com"
    },
    {
      "Country": "Austria",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Involve.Me"
    },
    {
      "Country": "Austria",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Emarsys.Com"
    },
    {
      "Country": "Austria",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Optimization Personalization & Testing",
      "URL": "Userbrain.Net"
    },
    {
      "Country": "Austria",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "Smart.Linkresearchtools.Com"
    },
    {
      "Country": "Austria",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Pyramidanalytics.Com"
    },
    {
      "Country": "Austria",
      "Landscape category": "Data",
      "Landscape subcategory": "Ipaas Cloud/Data Integration & Tag Management",
      "URL": "Jentis.Com"
    },
    {
      "Country": "Austria",
      "Landscape category": "Data",
      "Landscape subcategory": "Mobile & Web Analytics",
      "URL": "Amz.One"
    },
    {
      "Country": "Austria",
      "Landscape category": "Data",
      "Landscape subcategory": "Mobile & Web Analytics",
      "URL": "Heatclix.Net"
    },
    {
      "Country": "Austria",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Pagelanes.Com"
    },
    {
      "Country": "Azerbaijan",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Nextsale.Io"
    },
    {
      "Country": "Azerbaijan",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Live Chat & Chatbots",
      "URL": "Botboxpro.Com"
    },
    {
      "Country": "Belarus",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Nt.Technology"
    },
    {
      "Country": "Belarus",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Video Advertising",
      "URL": "Adsvideo.Co"
    },
    {
      "Country": "Belarus",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Amasty.Com"
    },
    {
      "Country": "Belarus",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Mobile Apps",
      "URL": "Flatlogic.Com"
    },
    {
      "Country": "Belarus",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Optimization Personalization & Testing",
      "URL": "Changeagain.Me"
    },
    {
      "Country": "Belarus",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "Link-Assistant.Com"
    },
    {
      "Country": "Belarus",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Stimulsoft.Com"
    },
    {
      "Country": "Belarus",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Ablebits.Com"
    },
    {
      "Country": "Belarus",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Kuku.Io"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Mobile Marketing",
      "URL": "Twipemobile.Com"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Mobile Marketing",
      "URL": "Hashting.Com"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Mobile Marketing",
      "URL": "Mobilosoft.Com"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Print",
      "URL": "Enfocus.Com"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Search & Social Advertising",
      "URL": "Marktwo.Be"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Channel Partner & Local Marketing",
      "URL": "Getseaters.Com"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Channel Partner & Local Marketing",
      "URL": "Proxistore.Com"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Retail Proximity & Iot Marketing",
      "URL": "Scandalook.Com"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Retail Proximity & Iot Marketing",
      "URL": "Chestnote.Com"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Prompto.Com"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Pimalion.Com"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Phished.Io"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Emailtree.Ai"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Luckycycle.Com"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Zetapul.Se"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Mobile Apps",
      "URL": "Twixlmedia.Com"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "Woorank.Com"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "Verbolia.Com"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "Semactic.Com"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Theoplayer.Com"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Storyme.Com"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Data",
      "Landscape subcategory": "Audience/Marketing Data & Data Enhancement",
      "URL": "Trendminer.Com"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Data",
      "Landscape subcategory": "Customer Data Platform",
      "URL": "Piximate.Net"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Data",
      "Landscape subcategory": "Dashboards & Data Visualization",
      "URL": "Datylon.Com"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Data",
      "Landscape subcategory": "Governance Compliance And Privacy",
      "URL": "Permissionmachine.Com"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Data",
      "Landscape subcategory": "Ipaas Cloud/Data Integration & Tag Management",
      "URL": "Nobi.Digital"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Data",
      "Landscape subcategory": "Ipaas Cloud/Data Integration & Tag Management",
      "URL": "Bleesk.Com"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Data",
      "Landscape subcategory": "Ipaas Cloud/Data Integration & Tag Management",
      "URL": "Apideck.Com"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Data",
      "Landscape subcategory": "Marketing Analytics Performance & Attribution",
      "URL": "Gatekeepr.Webflow.Io"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Data",
      "Landscape subcategory": "Mobile & Web Analytics",
      "URL": "Proxyclick.Com"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Management",
      "Landscape subcategory": "Budgeting & Finance",
      "URL": "Myfreedelity.Com"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Management",
      "Landscape subcategory": "Budgeting & Finance",
      "URL": "Dalenys.Com"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Management",
      "Landscape subcategory": "Projects & Workflow",
      "URL": "Dink.Eu"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Evoqia.Eu"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Sparkcentral.Com"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Getcardify.Com"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Invitedesk.Com"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Influencers",
      "URL": "Steller.Io"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Influencers",
      "URL": "Adshot.Io"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Live Chat & Chatbots",
      "URL": "Faqbot.Co"
    },
    {
      "Country": "Belgium",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Soprism.Com"
    },
    {
      "Country": "Bulgaria",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Watcherme.Com"
    },
    {
      "Country": "Bulgaria",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Metrilo.Com"
    },
    {
      "Country": "Bulgaria",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Smsbump.Com"
    },
    {
      "Country": "Bulgaria",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Perpetto.Com"
    },
    {
      "Country": "Bulgaria",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Telerik.Com"
    },
    {
      "Country": "Bulgaria",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Mediasolution3.Com"
    },
    {
      "Country": "Bulgaria",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Paradine.At"
    },
    {
      "Country": "Bulgaria",
      "Landscape category": "Management",
      "Landscape subcategory": "Agile & Lean Management",
      "URL": "Kanbanize.Com"
    },
    {
      "Country": "Bulgaria",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Advocacy Loyalty & Referrals",
      "URL": "Loyax.Com"
    },
    {
      "Country": "Croatia",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Mobile Marketing",
      "URL": "Exrey.Tv"
    },
    {
      "Country": "Croatia",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Retail Proximity & Iot Marketing",
      "URL": "Index.Hr"
    },
    {
      "Country": "Croatia",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Velebit.Ai"
    },
    {
      "Country": "Croatia",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Equinox.Vision"
    },
    {
      "Country": "Croatia",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Mediatoolkit.Com"
    },
    {
      "Country": "Cyprus",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Mobile Marketing",
      "URL": "Minimob.Com"
    },
    {
      "Country": "Cyprus",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Retail Proximity & Iot Marketing",
      "URL": "Miipharos.Com"
    },
    {
      "Country": "Cyprus",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Getsitecontrol.Com"
    },
    {
      "Country": "Cyprus",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Heyoliver.Com"
    },
    {
      "Country": "Cyprus",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "Finteza.Com"
    },
    {
      "Country": "Cyprus",
      "Landscape category": "Data",
      "Landscape subcategory": "Ipaas Cloud/Data Integration & Tag Management",
      "URL": "Justcontrol.It"
    },
    {
      "Country": "Czech Republic",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Adcash.Com"
    },
    {
      "Country": "Czech Republic",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Pr",
      "URL": "Prnews.Io"
    },
    {
      "Country": "Czech Republic",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Platforms & Carts",
      "URL": "Shoptet.Cz"
    },
    {
      "Country": "Czech Republic",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Salesbooster.Leadspicker.Com"
    },
    {
      "Country": "Czech Republic",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Kontent.Ai"
    },
    {
      "Country": "Czech Republic",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Xperience.Io"
    },
    {
      "Country": "Czech Republic",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Outfunnel.Com"
    },
    {
      "Country": "Czech Republic",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Motionlab.Io"
    },
    {
      "Country": "Czech Republic",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Leady.Com"
    },
    {
      "Country": "Czech Republic",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Getmanta.Com"
    },
    {
      "Country": "Czech Republic",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Monkeydata.Com"
    },
    {
      "Country": "Czech Republic",
      "Landscape category": "Data",
      "Landscape subcategory": "Ipaas Cloud/Data Integration & Tag Management",
      "URL": "Devart.Com"
    },
    {
      "Country": "Czech Republic",
      "Landscape category": "Data",
      "Landscape subcategory": "Ipaas Cloud/Data Integration & Tag Management",
      "URL": "Integromat.Com"
    },
    {
      "Country": "Czech Republic",
      "Landscape category": "Data",
      "Landscape subcategory": "Ipaas Cloud/Data Integration & Tag Management",
      "URL": "Skyvia.Com"
    },
    {
      "Country": "Czech Republic",
      "Landscape category": "Data",
      "Landscape subcategory": "Marketing Analytics Performance & Attribution",
      "URL": "Roivenue.Com"
    },
    {
      "Country": "Czech Republic",
      "Landscape category": "Data",
      "Landscape subcategory": "Marketing Analytics Performance & Attribution",
      "URL": "Roihunter.Com"
    },
    {
      "Country": "Czech Republic",
      "Landscape category": "Data",
      "Landscape subcategory": "Mobile & Web Analytics",
      "URL": "Smartlook.Com"
    },
    {
      "Country": "Czech Republic",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Tabidoo.Cloud"
    },
    {
      "Country": "Czech Republic",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Sprinxcrm.Com"
    },
    {
      "Country": "Czech Republic",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Satismeter.Com"
    },
    {
      "Country": "Czech Republic",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Eventee.Co"
    },
    {
      "Country": "Czech Republic",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Influencers",
      "URL": "Adhive.Tv"
    },
    {
      "Country": "Czech Republic",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Ritetag.Com"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Native/Content Advertising",
      "URL": "Passendo.Com"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Pr",
      "URL": "Hypefactors.Com"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Search & Social Advertising",
      "URL": "Confect.Io"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Clerk.Io"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Easysize.Me"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Platforms & Carts",
      "URL": "Dreamdata.Io"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Myphoner.Com"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Prezentor.Com"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Shieldapp.Ai"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Adversus.Io"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Conferize.Com"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Monsido.Com"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Sitecore.Com"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Jumpstory.Com"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Hesehus.Com"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Encode.Eu"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Kontainer.Com"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Posone.Eu"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Templafy.Com"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Yulsn.Com"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Optimization Personalization & Testing",
      "URL": "Monoloop.Com"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Optimization Personalization & Testing",
      "URL": "Siteimprove.Com"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Optimization Personalization & Testing",
      "URL": "Sleeknote.Com"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Fliva.Com"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Videojaguar.Com"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Twentythree.Net"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Data",
      "Landscape subcategory": "Audience/Marketing Data & Data Enhancement",
      "URL": "Ocean.Io"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Vertica.Com"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Data",
      "Landscape subcategory": "Customer Data Platform",
      "URL": "Flowstack.Com"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Data",
      "Landscape subcategory": "Customer Data Platform",
      "URL": "Custimy.Io"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Data",
      "Landscape subcategory": "Marketing Analytics Performance & Attribution",
      "URL": "Cavea.Io"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Data",
      "Landscape subcategory": "Mobile & Web Analytics",
      "URL": "Mouseflow.Com"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Management",
      "Landscape subcategory": "Agile & Lean Management",
      "URL": "Heylouise.App"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Management",
      "Landscape subcategory": "Agile & Lean Management",
      "URL": "Scrumwise.Com"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Actimo.Com"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Management",
      "Landscape subcategory": "Product Management",
      "URL": "Layerise.Com"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Management",
      "Landscape subcategory": "Projects & Workflow",
      "URL": "Forecast.App"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Conversational Marketing And Chat",
      "URL": "Botxo.Ai"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Webcrm.Com"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Realfiction.Com"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Tame.Events"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Influencers",
      "URL": "1Nfluencersmarketing.Com"
    },
    {
      "Country": "Denmark",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Influencers",
      "URL": "Influencermarketinghub.Com"
    },
    {
      "Country": "Estonia",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Voog.Com"
    },
    {
      "Country": "Estonia",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Wynter.Com"
    },
    {
      "Country": "Estonia",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Inwise.Com"
    },
    {
      "Country": "Estonia",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Smaily.Com"
    },
    {
      "Country": "Estonia",
      "Landscape category": "Data",
      "Landscape subcategory": "Mobile & Web Analytics",
      "URL": "Plausible.Io"
    },
    {
      "Country": "Estonia",
      "Landscape category": "Data",
      "Landscape subcategory": "Mobile & Web Analytics",
      "URL": "Reflectivedata.Com"
    },
    {
      "Country": "Estonia",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Fleep.Io"
    },
    {
      "Country": "Estonia",
      "Landscape category": "Management",
      "Landscape subcategory": "Product Management",
      "URL": "Nutiteq.Com"
    },
    {
      "Country": "Estonia",
      "Landscape category": "Management",
      "Landscape subcategory": "Projects & Workflow",
      "URL": "Toggl.Com"
    },
    {
      "Country": "Estonia",
      "Landscape category": "Management",
      "Landscape subcategory": "Talent Management",
      "URL": "Weekdone.Com"
    },
    {
      "Country": "Estonia",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Festivality.Co"
    },
    {
      "Country": "Estonia",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Influencers",
      "URL": "Modash.Io"
    },
    {
      "Country": "Estonia",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Influencers",
      "URL": "Adhive.Com"
    },
    {
      "Country": "Finland",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Mobile Marketing",
      "URL": "Exomi.Com"
    },
    {
      "Country": "Finland",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Print",
      "URL": "Tinkercad.Com"
    },
    {
      "Country": "Finland",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Retail Proximity & Iot Marketing",
      "URL": "Magicadd.Com"
    },
    {
      "Country": "Finland",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Neyra.Ai"
    },
    {
      "Country": "Finland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Hislide.Io"
    },
    {
      "Country": "Finland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Leadroid.Com"
    },
    {
      "Country": "Finland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Mobile Apps",
      "URL": "Forethink.Net"
    },
    {
      "Country": "Finland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Onestream.Live"
    },
    {
      "Country": "Finland",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Firstofficer.Io"
    },
    {
      "Country": "Finland",
      "Landscape category": "Data",
      "Landscape subcategory": "Customer Data Platform",
      "URL": "Gravito.Net"
    },
    {
      "Country": "Finland",
      "Landscape category": "Data",
      "Landscape subcategory": "Ipaas Cloud/Data Integration & Tag Management",
      "URL": "Frends.Com"
    },
    {
      "Country": "Finland",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Flowdock.Com"
    },
    {
      "Country": "Finland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Community & Reviews",
      "URL": "Fingertip.Org"
    },
    {
      "Country": "Finland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Community & Reviews",
      "URL": "Trustmary.Com"
    },
    {
      "Country": "Finland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Influencers",
      "URL": "Boksi.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Gamned.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Mobile Marketing",
      "URL": "Adikteev.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Mobile Marketing",
      "URL": "Moodmessenger.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Mobile Marketing",
      "URL": "Oktave.Co"
    },
    {
      "Country": "France",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Mobile Marketing",
      "URL": "Relatia.Fr"
    },
    {
      "Country": "France",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Pr",
      "URL": "Plussh.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Print",
      "URL": "Sculpteo.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Search & Social Advertising",
      "URL": "S4M.Io"
    },
    {
      "Country": "France",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Search & Social Advertising",
      "URL": "Emoteev.Io"
    },
    {
      "Country": "France",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Affiliate Marketing & Management",
      "URL": "Sharework.Co"
    },
    {
      "Country": "France",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Affiliate Marketing & Management",
      "URL": "Strackr.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Jvweb.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Datahawk.Co"
    },
    {
      "Country": "France",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Retail Proximity & Iot Marketing",
      "URL": "Ubiq-Social.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Retail Proximity & Iot Marketing",
      "URL": "Deepreach.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Retail Proximity & Iot Marketing",
      "URL": "Herow.Io"
    },
    {
      "Country": "France",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Retail Proximity & Iot Marketing",
      "URL": "Unitag.Io"
    },
    {
      "Country": "France",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Imaweb.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Lagrowthmachine.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Roadiz.Io"
    },
    {
      "Country": "France",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "B2Evolution.Net"
    },
    {
      "Country": "France",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Limber.Io"
    },
    {
      "Country": "France",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Doz.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "3C-E.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Agena3000.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Adiict.Fr"
    },
    {
      "Country": "France",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Afineo.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Onebase.Fr"
    },
    {
      "Country": "France",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Gridbees.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Quable.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Solidpepper.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Yalabot.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Wedia-Group.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Mailkitchen.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Acyba.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Touchify.Co"
    },
    {
      "Country": "France",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Iadvize.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Mobile Apps",
      "URL": "Goodbarber.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Mobile Apps",
      "URL": "Highconnexion.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Mobile Apps",
      "URL": "Voodoo.Io"
    },
    {
      "Country": "France",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Optimization Personalization & Testing",
      "URL": "D-Aim.Fr"
    },
    {
      "Country": "France",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "Botify.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Skewerlab.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Adways.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Playplay.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Api.Video"
    },
    {
      "Country": "France",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Aive.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Dolmen-Tech.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Data",
      "Landscape subcategory": "Dashboards & Data Visualization",
      "URL": "Clicdata.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Data",
      "Landscape subcategory": "Dmp",
      "URL": "Squadata.Net"
    },
    {
      "Country": "France",
      "Landscape category": "Data",
      "Landscape subcategory": "Governance Compliance And Privacy",
      "URL": "Didomi.Io"
    },
    {
      "Country": "France",
      "Landscape category": "Data",
      "Landscape subcategory": "Governance Compliance And Privacy",
      "URL": "Imatag.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Data",
      "Landscape subcategory": "Ipaas Cloud/Data Integration & Tag Management",
      "URL": "Pixelme.Me"
    },
    {
      "Country": "France",
      "Landscape category": "Data",
      "Landscape subcategory": "Marketing Analytics Performance & Attribution",
      "URL": "Ipsos.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Management",
      "Landscape subcategory": "Budgeting & Finance",
      "URL": "Pricinghub.Net"
    },
    {
      "Country": "France",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Iobeya.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Gladys.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Azendoo.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Agil.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Exoplatform.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Management",
      "Landscape subcategory": "Product Management",
      "URL": "Hubstairs.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Management",
      "Landscape subcategory": "Product Management",
      "URL": "Ar-Go.Co"
    },
    {
      "Country": "France",
      "Landscape category": "Management",
      "Landscape subcategory": "Product Management",
      "URL": "Emersya.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Management",
      "Landscape subcategory": "Projects & Workflow",
      "URL": "Slite.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Management",
      "Landscape subcategory": "Vendor Analysis",
      "URL": "Appvizer.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Management",
      "Landscape subcategory": "Vendor Analysis",
      "URL": "Itischaos.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Management",
      "Landscape subcategory": "Vendor Analysis",
      "URL": "Licence.One"
    },
    {
      "Country": "France",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Advocacy Loyalty & Referrals",
      "URL": "Spreadfamily.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Advocacy Loyalty & Referrals",
      "URL": "Weadvocacy.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Call Analytics & Management",
      "URL": "Callr.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Call Analytics & Management",
      "URL": "Aircall.Io"
    },
    {
      "Country": "France",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Call Analytics & Management",
      "URL": "Tritel.Nl"
    },
    {
      "Country": "France",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Call Analytics & Management",
      "URL": "Magnetis.Fr"
    },
    {
      "Country": "France",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Call Analytics & Management",
      "URL": "Ringover.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Community & Reviews",
      "URL": "Howtank.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Community & Reviews",
      "URL": "Clustdoc.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Cirrus-Shield.Fr"
    },
    {
      "Country": "France",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Koolcrm.Fr"
    },
    {
      "Country": "France",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Corymb.Us"
    },
    {
      "Country": "France",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Mindsay.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Mediatechsolution.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Odigo.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Livestorm.Co"
    },
    {
      "Country": "France",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Eventtia.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Arkadin.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Influencers",
      "URL": "Upfluence.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Influencers",
      "URL": "Moon.Xyz"
    },
    {
      "Country": "France",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Influencers",
      "URL": "Kolsquare.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Influencers",
      "URL": "Reachmaker.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Live Chat & Chatbots",
      "URL": "Tilkee.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Live Chat & Chatbots",
      "URL": "Sently.Io"
    },
    {
      "Country": "France",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Live Chat & Chatbots",
      "URL": "Oppscience.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Live Chat & Chatbots",
      "URL": "Greenbureau.Com"
    },
    {
      "Country": "France",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Live Chat & Chatbots",
      "URL": "Discussnow.Co"
    },
    {
      "Country": "Germany",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Hygh.Tech/De/"
    },
    {
      "Country": "Germany",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Sociomantic.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Mobile Marketing",
      "URL": "Addapptr.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Mobile Marketing",
      "URL": "Getpushmonkey.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Pr",
      "URL": "Openpr.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Print",
      "URL": "Priint.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Print",
      "URL": "Adscale.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Search & Social Advertising",
      "URL": "Cutnut.Net"
    },
    {
      "Country": "Germany",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Global-Savings-Group.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Platforms & Carts",
      "URL": "Billwerk.Io"
    },
    {
      "Country": "Germany",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Platforms & Carts",
      "URL": "Alzura.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Retail Proximity & Iot Marketing",
      "URL": "Record-Evolution.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Neuro-Flash.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Pitch.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Pricefx.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Echobot.Io"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Findologic.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Structr.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Contens.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Contao.Org"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Scompler.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Alugha.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Artist-Global.Biz"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Ax-Semantics.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Storyliner.Io"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Contentbird.Io"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Speechtext.Ai"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Ityxsolutions.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Ci-Hub.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Atrodam.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Brandification.Comen/"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Filestage.Io"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "40Three.Io"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "4Allportal.Net"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Ablex.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Kittelberger.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Amirada.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Incony.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Apollon.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Asim.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Atrify.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Aufwind-Group.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Brandbox.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Brickfox.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Commerceboard.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Syscon-Network.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Crossbase.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Mpdigital.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Data-Room.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Deltashops.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Dietz.Digital"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Eggheads.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Factorplus.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Ianeo.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Hoerl-Im.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Ifcc.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Infuniq.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Itb-Pim.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Jstage.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Lisocon.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "E-Sign.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Masterpim.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Mdc.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Bertschinnovation.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "E-Pro.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Novomind.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Noxum.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Pim.Red"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "P7Kommunikation.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Pimbase.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Pimics.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Pirobase-Imperia.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Cic.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Pressmind.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Prodexa.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Rasin.Eu"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Semantic-Pdm.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Six.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Soko.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Speed4Trade.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Ctrl-S.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Systrion.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Tradebyte.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Fischer-Information.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Atropim.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Ust-Gmbh.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Viamedici.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Myview.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Xara.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Www.Ci-Hub.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Brandad-Systems.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Marcapo.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Socoto.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Bertschinnovation.Com/En/Product/Mediacockpit/"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Sendeffect.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "De.Sendinblue.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Mailtastic.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Mailingwork.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Cleverelements.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Agnitas.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Mackevision.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Snoopstar.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Interactivepaper.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Aidaform.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Cogia.Deen/"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Jens.Marketing/En/Comment-Like-As-Linkedin-Company-Page/"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Aioma.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Whiterabbitsuite.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Thorit.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Promio.Net"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Dymatrix.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Connectedware.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Cleverworks.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Chocobrain.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Chimpify.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Artegic.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Mobile Apps",
      "URL": "Kayzen.Io/"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Optimization Personalization & Testing",
      "URL": "Proadly.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Optimization Personalization & Testing",
      "URL": "Saucelabs.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Smartmobilelabs.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Instalod.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Segmentive.Ai"
    },
    {
      "Country": "Germany",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Demoup.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Quantifynow.App/"
    },
    {
      "Country": "Germany",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Infozoom.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Airnowdata.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Data",
      "Landscape subcategory": "Customer Data Platform",
      "URL": "Cxomni.Net"
    },
    {
      "Country": "Germany",
      "Landscape category": "Data",
      "Landscape subcategory": "Dashboards & Data Visualization",
      "URL": "Delta42.Io/En/Home/"
    },
    {
      "Country": "Germany",
      "Landscape category": "Data",
      "Landscape subcategory": "Marketing Analytics Performance & Attribution",
      "URL": "Swaarm.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Data",
      "Landscape subcategory": "Mobile & Web Analytics",
      "URL": "Econda.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Management",
      "Landscape subcategory": "Budgeting & Finance",
      "URL": "Lean-Case.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Circuit.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Brandguardian.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Mindmeister.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Organisemee.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Paperfly.Io"
    },
    {
      "Country": "Germany",
      "Landscape category": "Management",
      "Landscape subcategory": "Product Management",
      "URL": "Productsup.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Call Analytics & Management",
      "URL": "Matelso.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Call Analytics & Management",
      "URL": "Getciara.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Call Analytics & Management",
      "URL": "Babelforce.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Community & Reviews",
      "URL": "Usetrust.Io/"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Wice.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Vertec.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Softfolio.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Snapaddy.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Pisasales.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Gedys-Intraware.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Flowfact.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Daypaio.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Cursor.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Viennaadvantage.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Combit.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Centralstationcrm.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Bsigroup.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Amtangee.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "1Salescrm.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Userlane.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Promoter.Ninja"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Feedbackstr.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Bookingkit.Net"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Onapply.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Slidepresenter.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Storybox.Cloud"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Demodesk.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Influencers",
      "URL": "Influencerdb.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Live Chat & Chatbots",
      "URL": "E-Bot7.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Live Chat & Chatbots",
      "URL": "Botoni.De"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Live Chat & Chatbots",
      "URL": "Messengerpeople.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Digimind.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Sharekit.Io"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Rascasse.Com"
    },
    {
      "Country": "Germany",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Combin.Com"
    },
    {
      "Country": "Greece",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Fighthoax.Com"
    },
    {
      "Country": "Greece",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Mobile Marketing",
      "URL": "Cytechmobile.Com"
    },
    {
      "Country": "Greece",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Proudsyrup.Com"
    },
    {
      "Country": "Greece",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Pollfish.Com"
    },
    {
      "Country": "Greece",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Woorise.Com"
    },
    {
      "Country": "Greece",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Contactpigeon.Com"
    },
    {
      "Country": "Greece",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Reembed.Com"
    },
    {
      "Country": "Greece",
      "Landscape category": "Data",
      "Landscape subcategory": "Audience/Marketing Data & Data Enhancement",
      "URL": "Convertgroup.Com"
    },
    {
      "Country": "Greece",
      "Landscape category": "Data",
      "Landscape subcategory": "Mobile & Web Analytics",
      "URL": "Loceye.Io"
    },
    {
      "Country": "Greece",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Advocacy Loyalty & Referrals",
      "URL": "Warp.Ly"
    },
    {
      "Country": "Hungary",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Prezi.Com"
    },
    {
      "Country": "Hungary",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Neticle.Com"
    },
    {
      "Country": "Hungary",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Keytiles.Com"
    },
    {
      "Country": "Hungary",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Automizy.Com"
    },
    {
      "Country": "Hungary",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Maileon.Co.Uk"
    },
    {
      "Country": "Hungary",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Edmdesigner.Com"
    },
    {
      "Country": "Hungary",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Zurvey.Io"
    },
    {
      "Country": "Hungary",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Mobile Apps",
      "URL": "Bitrise.Io"
    },
    {
      "Country": "Hungary",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Optimization Personalization & Testing",
      "URL": "Optimonk.Com"
    },
    {
      "Country": "Hungary",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Live Chat & Chatbots",
      "URL": "Talkabot.Net"
    },
    {
      "Country": "Iceland",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Crankwheel.Com"
    },
    {
      "Country": "Iceland",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Tagplay.Co"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Blockthrough.Com"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Mobile Marketing",
      "URL": "Cadoo.Com"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Mobile Marketing",
      "URL": "Velti.Com"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Mobile Marketing",
      "URL": "Heystaks.Com"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Native/Content Advertising",
      "URL": "Chameleon.Ad"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Content Marketing",
      "URL": "3Dissue.Com"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Trust-Minder.Com"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Channelsight.Com"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Platforms & Carts",
      "URL": "Profitero.Com"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Platforms & Carts",
      "URL": "2Checkout.Com"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Visiblethread.Com"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Datahug.Com"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Channel Partner & Local Marketing",
      "URL": "Channelmechanics.Com"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Terminalfour.Com"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Appdrag.Com"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Rebrandly.Com"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Visua.Com"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Crowdsignal.Com"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Xtremepush.Com"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Mobile Apps",
      "URL": "Openback.Com"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Fluix.Io"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "Monitorbacklinks.Com"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Data",
      "Landscape subcategory": "Governance Compliance And Privacy",
      "URL": "Planetverify.Com"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Data",
      "Landscape subcategory": "Marketing Analytics Performance & Attribution",
      "URL": "Usheru.Com"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Management",
      "Landscape subcategory": "Agile & Lean Management",
      "URL": "Teamwork.Com"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Management",
      "Landscape subcategory": "Talent Management",
      "URL": "Advisable.Com"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Advocacy Loyalty & Referrals",
      "URL": "Loylap.Com"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Advocacy Loyalty & Referrals",
      "URL": "Azpiral.Com"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Advocacy Loyalty & Referrals",
      "URL": "Workvivo.Com"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Live Chat & Chatbots",
      "URL": "Voysis.Com"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Gladcloud.Mobi"
    },
    {
      "Country": "Ireland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Video Marketing",
      "URL": "Videosherpa.Com"
    },
    {
      "Country": "Israel",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Segmanta.Com"
    },
    {
      "Country": "Italy",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Mobile Marketing",
      "URL": "Nearit.Com"
    },
    {
      "Country": "Italy",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Retail Proximity & Iot Marketing",
      "URL": "Pangearetail.Com"
    },
    {
      "Country": "Italy",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Wim.Tv"
    },
    {
      "Country": "Italy",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Keanet.It"
    },
    {
      "Country": "Italy",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Ekr.It"
    },
    {
      "Country": "Italy",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "4-Flying.Com"
    },
    {
      "Country": "Italy",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Adabra.Com"
    },
    {
      "Country": "Italy",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Mobile Apps",
      "URL": "Beintoo.Com"
    },
    {
      "Country": "Italy",
      "Landscape category": "Data",
      "Landscape subcategory": "Audience/Marketing Data & Data Enhancement",
      "URL": "Egon.Com"
    },
    {
      "Country": "Italy",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Spagobi.Org"
    },
    {
      "Country": "Italy",
      "Landscape category": "Data",
      "Landscape subcategory": "Dashboards & Data Visualization",
      "URL": "Askdata.Com"
    },
    {
      "Country": "Italy",
      "Landscape category": "Data",
      "Landscape subcategory": "Ipaas Cloud/Data Integration & Tag Management",
      "URL": "Nocoding.It"
    },
    {
      "Country": "Italy",
      "Landscape category": "Data",
      "Landscape subcategory": "Mobile & Web Analytics",
      "URL": "Clickmeter.Com"
    },
    {
      "Country": "Italy",
      "Landscape category": "Management",
      "Landscape subcategory": "Talent Management",
      "URL": "Ncorehr.Com"
    },
    {
      "Country": "Italy",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Call Analytics & Management",
      "URL": "Kaleyra.Com"
    },
    {
      "Country": "Italy",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Klondike.Ai"
    },
    {
      "Country": "Italy",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Xcally.Com"
    },
    {
      "Country": "Italy",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Lifedata.Ai"
    },
    {
      "Country": "Italy",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Metooo.Io"
    },
    {
      "Country": "Italy",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Influencers",
      "URL": "Tools.Worldz.Net"
    },
    {
      "Country": "Latvia",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Setupad.Com"
    },
    {
      "Country": "Latvia",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Infogram.Com"
    },
    {
      "Country": "Latvia",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Onlyoffice.Com"
    },
    {
      "Country": "Latvia",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Call Analytics & Management",
      "URL": "Recorn.App"
    },
    {
      "Country": "Latvia",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Live Chat & Chatbots",
      "URL": "Typbot.Com"
    },
    {
      "Country": "Lithuania",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Search & Social Advertising",
      "URL": "Affise.Com"
    },
    {
      "Country": "Lithuania",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Search & Social Advertising",
      "URL": "Advertsup.Lt"
    },
    {
      "Country": "Lithuania",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Impresspages.Org"
    },
    {
      "Country": "Lithuania",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Mailerlite.Com"
    },
    {
      "Country": "Lithuania",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Sender.Net"
    },
    {
      "Country": "Lithuania",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Special-App.Com"
    },
    {
      "Country": "Lithuania",
      "Landscape category": "Data",
      "Landscape subcategory": "Marketing Analytics Performance & Attribution",
      "URL": "Redtrack.Io"
    },
    {
      "Country": "Lithuania",
      "Landscape category": "Management",
      "Landscape subcategory": "Projects & Workflow",
      "URL": "Eylean.Com"
    },
    {
      "Country": "Lithuania",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Myownconference.Com"
    },
    {
      "Country": "Luxembourg",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Mobile Marketing",
      "URL": "Olamobile.Com"
    },
    {
      "Country": "Luxembourg",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Simpleshow.Com"
    },
    {
      "Country": "Luxembourg",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Mood-Me.Com"
    },
    {
      "Country": "Luxembourg",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Atento.Com"
    },
    {
      "Country": "Malta",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Thrivelabs.Io"
    },
    {
      "Country": "Malta",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Affiliate Marketing & Management",
      "URL": "Netrefer.Com"
    },
    {
      "Country": "Malta",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Flippingbook.Com"
    },
    {
      "Country": "Malta",
      "Landscape category": "Data",
      "Landscape subcategory": "Mobile & Web Analytics",
      "URL": "Hotjar.Com"
    },
    {
      "Country": "Malta",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Advocacy Loyalty & Referrals",
      "URL": "Harviist.Com"
    },
    {
      "Country": "Moldova",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Zeroqode.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Adformatic.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Useadspace.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Affiliate Marketing & Management",
      "URL": "Localonlinemarketing.Nl"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Affiliate Marketing & Management",
      "URL": "Affiliated.Io"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Channel Partner & Local Marketing",
      "URL": "Channext.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Channel Partner & Local Marketing",
      "URL": "Mijnwebwinkel.Nl"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Channel Partner & Local Marketing",
      "URL": "Tiekinetix.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Platforms & Carts",
      "URL": "Propellor.Eu"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Platforms & Carts",
      "URL": "Nearchao.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Scaura.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Sales.Rocks"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Getshaman.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Aithena.Ai"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Snakeware.Nl"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Redaktcms.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Alpha.One"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Powertext.Ai"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Tailo.Nl"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Worldofcontent.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Getrevue.Co"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Limk.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Crm",
      "URL": "Yoobicrm.Nl"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Swivle.Nl"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Katanapim.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Logres.Nl"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Greedybean.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Xiam.Nl"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Tritac.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Novulo.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Web-Pixels.Nl"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Shoxl.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Skwirrel.Eu"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Distridata.Nl"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Taspac.Nl"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Flowmailer.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Cleeng.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Enormail.Eu"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Zivver.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Expivi.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Feedbackcompany.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Gobright.Ai"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Voxvote.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Onlinequizcreator.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Teamscopeapp.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Rockefy.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Leadelephant.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Xzazu.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Webpower.Nl"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Teamitg.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Mobile Apps",
      "URL": "Sharewire.Nl"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Mobile Apps",
      "URL": "Mobilebridge.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Retail Proximity & Iot Marketing",
      "URL": "Flatmedia.Nl"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "Wordproof.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "Siteguru.Co"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Triple8.Tv"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "24Sessions.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Data",
      "Landscape subcategory": "Audience/Marketing Data & Data Enhancement",
      "URL": "Fetchmore.Nl"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "S6.Io"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Carto.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Underlined.Nl"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Data",
      "Landscape subcategory": "Customer Data Platform",
      "URL": "Effectiveprofiles.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Data",
      "Landscape subcategory": "Customer Data Platform",
      "URL": "Milkymap.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Data",
      "Landscape subcategory": "Customer Data Platform",
      "URL": "Withzoey.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Data",
      "Landscape subcategory": "Dashboards & Data Visualization",
      "URL": "F19.Nl"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Data",
      "Landscape subcategory": "Dashboards & Data Visualization",
      "URL": "Whatagraph.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Data",
      "Landscape subcategory": "Governance Compliance And Privacy",
      "URL": "Ekata.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Data",
      "Landscape subcategory": "Ipaas Cloud/Data Integration & Tag Management",
      "URL": "Aplynk.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Data",
      "Landscape subcategory": "Ipaas Cloud/Data Integration & Tag Management",
      "URL": "Wem.Io"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Data",
      "Landscape subcategory": "Ipaas Cloud/Data Integration & Tag Management",
      "URL": "Bettyblocks.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Data",
      "Landscape subcategory": "Ipaas Cloud/Data Integration & Tag Management",
      "URL": "Thinkwisesoftware.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Data",
      "Landscape subcategory": "Marketing Analytics Performance & Attribution",
      "URL": "Dynata.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Data",
      "Landscape subcategory": "Marketing Analytics Performance & Attribution",
      "URL": "Adcurve.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Data",
      "Landscape subcategory": "Marketing Analytics Performance & Attribution",
      "URL": "Metrixlab.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Data",
      "Landscape subcategory": "Mobile & Web Analytics",
      "URL": "Smartocto.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Management",
      "Landscape subcategory": "Agile & Lean Management",
      "URL": "Sprintground.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Fenestrae.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Klippa.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Management",
      "Landscape subcategory": "Product Management",
      "URL": "Cobrowser.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Management",
      "Landscape subcategory": "Projects & Workflow",
      "URL": "Zummit.Nl"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Management",
      "Landscape subcategory": "Projects & Workflow",
      "URL": "Timechimp.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Management",
      "Landscape subcategory": "Projects & Workflow",
      "URL": "Tabbli.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Management",
      "Landscape subcategory": "Talent Management",
      "URL": "Sortlist.Nl"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Management",
      "Landscape subcategory": "Talent Management",
      "URL": "Knowingo.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Management",
      "Landscape subcategory": "Talent Management",
      "URL": "Collabdo.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Advocacy Loyalty & Referrals",
      "URL": "Ok.App"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Call Analytics & Management",
      "URL": "Gntel.Nl"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Community & Reviews",
      "URL": "Nabble.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Community & Reviews",
      "URL": "Cmnty.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Getoiio.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Tribecrm.Nl"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Avisi.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Perfectview.Nl"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Kaizo.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Pushcall.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Sentiencelab.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Upvoty.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Insocial.Eu"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Webinargeek.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "River.Fm"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Riverside.Fm"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Presentain.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Influencers",
      "URL": "Influentials.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Live Chat & Chatbots",
      "URL": "Userguest.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Live Chat & Chatbots",
      "URL": "Tripetto.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Apostlesocial.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Tweetfavy.Com"
    },
    {
      "Country": "Netherlands",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Repuso.Com"
    },
    {
      "Country": "Norway",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Adtube.No"
    },
    {
      "Country": "Norway",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Native/Content Advertising",
      "URL": "Kobler.No"
    },
    {
      "Country": "Norway",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Print",
      "URL": "Never.No"
    },
    {
      "Country": "Norway",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Print",
      "URL": "Isave.No"
    },
    {
      "Country": "Norway",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Search & Social Advertising",
      "URL": "Socius.Co"
    },
    {
      "Country": "Norway",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Affiliate Marketing & Management",
      "URL": "Targetcircle.Com"
    },
    {
      "Country": "Norway",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Platforms & Carts",
      "URL": "24Nettbutikk.No"
    },
    {
      "Country": "Norway",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Platforms & Carts",
      "URL": "Storeshop.No"
    },
    {
      "Country": "Norway",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Platforms & Carts",
      "URL": "Omnium.No"
    },
    {
      "Country": "Norway",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Salesscreen.Com"
    },
    {
      "Country": "Norway",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Simplifai.Ai"
    },
    {
      "Country": "Norway",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Webnodes.Com"
    },
    {
      "Country": "Norway",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Vev.Design"
    },
    {
      "Country": "Norway",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Enonic.Com"
    },
    {
      "Country": "Norway",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Sanity.Io"
    },
    {
      "Country": "Norway",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Linkmobility.Com"
    },
    {
      "Country": "Norway",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Fotoware.Com"
    },
    {
      "Country": "Norway",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Leadx360.Com"
    },
    {
      "Country": "Norway",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Loopify.Com"
    },
    {
      "Country": "Norway",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Vibbio.Com"
    },
    {
      "Country": "Norway",
      "Landscape category": "Data",
      "Landscape subcategory": "Audience/Marketing Data & Data Enhancement",
      "URL": "Nordicdataresources.Com"
    },
    {
      "Country": "Norway",
      "Landscape category": "Data",
      "Landscape subcategory": "Dashboards & Data Visualization",
      "URL": "Highcharts.Com"
    },
    {
      "Country": "Norway",
      "Landscape category": "Data",
      "Landscape subcategory": "Ipaas Cloud/Data Integration & Tag Management",
      "URL": "Chills.Cloud"
    },
    {
      "Country": "Norway",
      "Landscape category": "Management",
      "Landscape subcategory": "Budgeting & Finance",
      "URL": "Fygi.Io"
    },
    {
      "Country": "Norway",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Easit.No"
    },
    {
      "Country": "Norway",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Upwave.Io"
    },
    {
      "Country": "Norway",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Correlate.Com"
    },
    {
      "Country": "Norway",
      "Landscape category": "Management",
      "Landscape subcategory": "Talent Management",
      "URL": "Poption.Com"
    },
    {
      "Country": "Norway",
      "Landscape category": "Management",
      "Landscape subcategory": "Talent Management",
      "URL": "Preppio.Com"
    },
    {
      "Country": "Norway",
      "Landscape category": "Management",
      "Landscape subcategory": "Talent Management",
      "URL": "Nixa.Io"
    },
    {
      "Country": "Norway",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Advocacy Loyalty & Referrals",
      "URL": "Placewise.Com"
    },
    {
      "Country": "Norway",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Conversational Marketing And Chat",
      "URL": "Cavai.Com"
    },
    {
      "Country": "Norway",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Headshed.Com"
    },
    {
      "Country": "Norway",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Omnicus.Com"
    },
    {
      "Country": "Norway",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Sharefox.No"
    },
    {
      "Country": "Norway",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Confrere.Com"
    },
    {
      "Country": "Norway",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Easymeeting.Net"
    },
    {
      "Country": "Norway",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Checkin.No"
    },
    {
      "Country": "Norway",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Start.Ticketco.Events"
    },
    {
      "Country": "Norway",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Hoopla.No"
    },
    {
      "Country": "Norway",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Makeplans.Net"
    },
    {
      "Country": "Norway",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Meltwater.Com"
    },
    {
      "Country": "Norway",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Socialboards.Com"
    },
    {
      "Country": "Norway",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Movi.Ai"
    },
    {
      "Country": "Poland",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "E-Contenta.Com"
    },
    {
      "Country": "Poland",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Rtbhouse.Com"
    },
    {
      "Country": "Poland",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Mobile Marketing",
      "URL": "Kontakt.Io"
    },
    {
      "Country": "Poland",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Video Advertising",
      "URL": "Videommerce.Com"
    },
    {
      "Country": "Poland",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Affiliate Marketing & Management",
      "URL": "Mylead.Global"
    },
    {
      "Country": "Poland",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Crm",
      "URL": "Instream.Io"
    },
    {
      "Country": "Poland",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "100Shoppers.Com"
    },
    {
      "Country": "Poland",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Samito.Co"
    },
    {
      "Country": "Poland",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Platforms & Carts",
      "URL": "Vuestorefront.Io"
    },
    {
      "Country": "Poland",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Platforms & Carts",
      "URL": "Idosell.Com"
    },
    {
      "Country": "Poland",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Platforms & Carts",
      "URL": "Shoplo.Com"
    },
    {
      "Country": "Poland",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Platforms & Carts",
      "URL": "Shoper.Pl"
    },
    {
      "Country": "Poland",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Retail Proximity & Iot Marketing",
      "URL": "Scanandbuy.Me"
    },
    {
      "Country": "Poland",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Closer.App"
    },
    {
      "Country": "Poland",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Apropo.Io"
    },
    {
      "Country": "Poland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Descra.Com"
    },
    {
      "Country": "Poland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Usebouncer.Com"
    },
    {
      "Country": "Poland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Emaillabs.Io"
    },
    {
      "Country": "Poland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Optimization Personalization & Testing",
      "URL": "Omniconvert.Com"
    },
    {
      "Country": "Poland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Optimization Personalization & Testing",
      "URL": "Userpeek.Com"
    },
    {
      "Country": "Poland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "Seodity.Com"
    },
    {
      "Country": "Poland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "Surferseo.Com"
    },
    {
      "Country": "Poland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Viralseed.Com"
    },
    {
      "Country": "Poland",
      "Landscape category": "Data",
      "Landscape subcategory": "Audience/Marketing Data & Data Enhancement",
      "URL": "Synerise.Com"
    },
    {
      "Country": "Poland",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Qed.Pl"
    },
    {
      "Country": "Poland",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Takecare.Ai"
    },
    {
      "Country": "Poland",
      "Landscape category": "Data",
      "Landscape subcategory": "Dashboards & Data Visualization",
      "URL": "Querona.Io"
    },
    {
      "Country": "Poland",
      "Landscape category": "Data",
      "Landscape subcategory": "Mobile & Web Analytics",
      "URL": "Livesession.Io"
    },
    {
      "Country": "Poland",
      "Landscape category": "Management",
      "Landscape subcategory": "Agile & Lean Management",
      "URL": "Kanbantool.Com"
    },
    {
      "Country": "Poland",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Reactivepad.Com"
    },
    {
      "Country": "Poland",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Taskbeat.Com"
    },
    {
      "Country": "Poland",
      "Landscape category": "Management",
      "Landscape subcategory": "Vendor Analysis",
      "URL": "Saasgenius.Com"
    },
    {
      "Country": "Poland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Edrone.Me"
    },
    {
      "Country": "Poland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Livespace.Io"
    },
    {
      "Country": "Poland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Livecall.Io"
    },
    {
      "Country": "Poland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Survicate.Com"
    },
    {
      "Country": "Poland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Influencers",
      "URL": "Connecsi.Com"
    },
    {
      "Country": "Poland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Live Chat & Chatbots",
      "URL": "Livechat.Com"
    },
    {
      "Country": "Poland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Live Chat & Chatbots",
      "URL": "Kodabots.Com"
    },
    {
      "Country": "Poland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Sentione.Com"
    },
    {
      "Country": "Poland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Comarch.Com"
    },
    {
      "Country": "Poland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Rocketlink.Io"
    },
    {
      "Country": "Poland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Brand24.Com"
    },
    {
      "Country": "Poland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Postfity.Com"
    },
    {
      "Country": "Portugal",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Mobile Marketing",
      "URL": "Onliquid.Com"
    },
    {
      "Country": "Portugal",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Search & Social Advertising",
      "URL": "Shiftforward.Eu"
    },
    {
      "Country": "Portugal",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Platforms & Carts",
      "URL": "Advertio.Com"
    },
    {
      "Country": "Portugal",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Trybugle.Com"
    },
    {
      "Country": "Portugal",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Unbabel.Com"
    },
    {
      "Country": "Portugal",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Crm",
      "URL": "Unmaze.Io"
    },
    {
      "Country": "Portugal",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Survs.Com"
    },
    {
      "Country": "Portugal",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Mobile Apps",
      "URL": "Codavel.Com"
    },
    {
      "Country": "Portugal",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "Beamusup.Com"
    },
    {
      "Country": "Portugal",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Muse.Ai"
    },
    {
      "Country": "Portugal",
      "Landscape category": "Data",
      "Landscape subcategory": "Dashboards & Data Visualization",
      "URL": "Viurdata.Com"
    },
    {
      "Country": "Portugal",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Planless.Io"
    },
    {
      "Country": "Portugal",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Bindtuning.Com"
    },
    {
      "Country": "Portugal",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Getsocial.Io"
    },
    {
      "Country": "Romania",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Morphl.Io"
    },
    {
      "Country": "Romania",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Pleisty.Com"
    },
    {
      "Country": "Romania",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Retail Proximity & Iot Marketing",
      "URL": "Wyliodrin.Com"
    },
    {
      "Country": "Romania",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Froala.Com"
    },
    {
      "Country": "Romania",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Pixteller.Com"
    },
    {
      "Country": "Romania",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Conectoo.Ro"
    },
    {
      "Country": "Romania",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "123Formbuilder.Com"
    },
    {
      "Country": "Romania",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Esurveyspro.Com"
    },
    {
      "Country": "Romania",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Widgetic.Com"
    },
    {
      "Country": "Romania",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "Advancedwebranking.Com"
    },
    {
      "Country": "Romania",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "Caphyon.Com"
    },
    {
      "Country": "Romania",
      "Landscape category": "Management",
      "Landscape subcategory": "Projects & Workflow",
      "URL": "Paymoapp.Com"
    },
    {
      "Country": "Romania",
      "Landscape category": "Management",
      "Landscape subcategory": "Projects & Workflow",
      "URL": "Rationalplan.Com"
    },
    {
      "Country": "Romania",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Community & Reviews",
      "URL": "Feedcheck.Co"
    },
    {
      "Country": "Romania",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Atollon.Com"
    },
    {
      "Country": "Romania",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Zimplu.Com"
    },
    {
      "Country": "Romania",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Socialinsider.Io"
    },
    {
      "Country": "Romania",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Brandmentions.Com"
    },
    {
      "Country": "Russia",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Firstdata.Io"
    },
    {
      "Country": "Russia",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Channel Partner & Local Marketing",
      "URL": "Pay2U.Ru"
    },
    {
      "Country": "Russia",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Platforms & Carts",
      "URL": "X-Cart.Com"
    },
    {
      "Country": "Russia",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Platforms & Carts",
      "URL": "Ecwid.Com"
    },
    {
      "Country": "Russia",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Gitlean.Com"
    },
    {
      "Country": "Russia",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Ucoz.Com"
    },
    {
      "Country": "Russia",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Bintime.Com"
    },
    {
      "Country": "Russia",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Arzilla.Io"
    },
    {
      "Country": "Russia",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Convertful.Com"
    },
    {
      "Country": "Russia",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Staply.Co"
    },
    {
      "Country": "Russia",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Altkraft.Com"
    },
    {
      "Country": "Russia",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Leadonance.Com"
    },
    {
      "Country": "Russia",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Ukit.Com"
    },
    {
      "Country": "Russia",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "Rankinity.Com"
    },
    {
      "Country": "Russia",
      "Landscape category": "Data",
      "Landscape subcategory": "Audience/Marketing Data & Data Enhancement",
      "URL": "Visto.Li/"
    },
    {
      "Country": "Russia",
      "Landscape category": "Data",
      "Landscape subcategory": "Audience/Marketing Data & Data Enhancement",
      "URL": "Metacommerce.Ru"
    },
    {
      "Country": "Russia",
      "Landscape category": "Data",
      "Landscape subcategory": "Customer Data Platform",
      "URL": "Addreality.Com"
    },
    {
      "Country": "Russia",
      "Landscape category": "Management",
      "Landscape subcategory": "Budgeting & Finance",
      "URL": "Netopia-Payments.Com"
    },
    {
      "Country": "Russia",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Advocacy Loyalty & Referrals",
      "URL": "Adcorn.Ru"
    },
    {
      "Country": "Russia",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Advocacy Loyalty & Referrals",
      "URL": "Flocktory.Com"
    },
    {
      "Country": "Russia",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Community & Reviews",
      "URL": "Copiny.Com"
    },
    {
      "Country": "Russia",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Wazzup24.Com"
    },
    {
      "Country": "Russia",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Flowlu.Com"
    },
    {
      "Country": "Russia",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Re-Desk.Com"
    },
    {
      "Country": "Russia",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Trueconf.Com"
    },
    {
      "Country": "Russia",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Live Chat & Chatbots",
      "URL": "Webim.Ru"
    },
    {
      "Country": "Russia",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Leadenforce.Com"
    },
    {
      "Country": "Scotland",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Weatherads.Io"
    },
    {
      "Country": "Scotland",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Platforms & Carts",
      "URL": "Adimo.Co"
    },
    {
      "Country": "Scotland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Snappd.Tv"
    },
    {
      "Country": "Scotland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Community & Reviews",
      "URL": "Opinew.Com"
    },
    {
      "Country": "Scotland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Mycustomerlens.Com"
    },
    {
      "Country": "Serbia",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Price2Spy.Com"
    },
    {
      "Country": "Serbia",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Intelisale.Com"
    },
    {
      "Country": "Serbia",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Skylead.Io"
    },
    {
      "Country": "Serbia",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Videobolt.Net"
    },
    {
      "Country": "Serbia",
      "Landscape category": "Data",
      "Landscape subcategory": "Audience/Marketing Data & Data Enhancement",
      "URL": "Adwhistle.Com"
    },
    {
      "Country": "Serbia",
      "Landscape category": "Management",
      "Landscape subcategory": "Agile & Lean Management",
      "URL": "Vivifyscrum.Com"
    },
    {
      "Country": "Serbia",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Activecollab.Com"
    },
    {
      "Country": "Serbia",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Community & Reviews",
      "URL": "Thepidemic.Com"
    },
    {
      "Country": "Slovakia",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Channel Partner & Local Marketing",
      "URL": "Localizationguru.Com"
    },
    {
      "Country": "Slovakia",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Retail Proximity & Iot Marketing",
      "URL": "Pygmalios.Com"
    },
    {
      "Country": "Slovakia",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "Kwfinder.Com"
    },
    {
      "Country": "Slovakia",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "Mangools.Com"
    },
    {
      "Country": "Slovakia",
      "Landscape category": "Data",
      "Landscape subcategory": "Marketing Analytics Performance & Attribution",
      "URL": "Biotron.Io"
    },
    {
      "Country": "Slovakia",
      "Landscape category": "Management",
      "Landscape subcategory": "Agile & Lean Management",
      "URL": "Scrumdesk.Com"
    },
    {
      "Country": "Slovakia",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Bind.Rs"
    },
    {
      "Country": "Slovakia",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Kontentino.Com"
    },
    {
      "Country": "Slovakia",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Nicereply.Com"
    },
    {
      "Country": "Slovenia",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Channel Partner & Local Marketing",
      "URL": "Urbanapp.Net"
    },
    {
      "Country": "Slovenia",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Justmessenger.Net"
    },
    {
      "Country": "Slovenia",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Squalomail.Com"
    },
    {
      "Country": "Slovenia",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Zebrabi.Com"
    },
    {
      "Country": "Slovenia",
      "Landscape category": "Management",
      "Landscape subcategory": "Projects & Workflow",
      "URL": "Myhours.Com"
    },
    {
      "Country": "Slovenia",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Intrixcrm.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Seedtag.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Acuityads.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Mobile Marketing",
      "URL": "Pushed.Co"
    },
    {
      "Country": "Spain",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Mobile Marketing",
      "URL": "Moca-Pm.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Mobile Marketing",
      "URL": "Mediasmart.Io"
    },
    {
      "Country": "Spain",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Inbenta.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Blueknow.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Platforms & Carts",
      "URL": "Openbravo.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Platforms & Carts",
      "URL": "Elcodi.Io"
    },
    {
      "Country": "Spain",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Retail Proximity & Iot Marketing",
      "URL": "Taptapnetworks.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Retail Proximity & Iot Marketing",
      "URL": "Gonowpro.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Xeerpa.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Refract.Ai"
    },
    {
      "Country": "Spain",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Pyrocms.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Navigatecms.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Answerbase.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Frizbit.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Pleasepoint.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Mobile Apps",
      "URL": "Kingofapp.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Optimization Personalization & Testing",
      "URL": "Neliosoftware.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Pirsonal.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Data",
      "Landscape subcategory": "Audience/Marketing Data & Data Enhancement",
      "URL": "Delidatax.Net"
    },
    {
      "Country": "Spain",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Nicepeopleatwork.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Beabloo.Com/"
    },
    {
      "Country": "Spain",
      "Landscape category": "Data",
      "Landscape subcategory": "Dashboards & Data Visualization",
      "URL": "Flameanalytics.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Data",
      "Landscape subcategory": "Governance Compliance And Privacy",
      "URL": "Hetikus.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Data",
      "Landscape subcategory": "Mobile & Web Analytics",
      "URL": "Easypromosapp.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Data",
      "Landscape subcategory": "Mobile & Web Analytics",
      "URL": "Openwebanalytics.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Management",
      "Landscape subcategory": "Product Management",
      "URL": "Saleslayer.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Management",
      "Landscape subcategory": "Vendor Analysis",
      "URL": "Getapp.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Advocacy Loyalty & Referrals",
      "URL": "Redkoalia.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Community & Reviews",
      "URL": "Reviewpro.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Sumacrm.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Videoask.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Live Chat & Chatbots",
      "URL": "Landbot.Io"
    },
    {
      "Country": "Spain",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Sentisis.Com"
    },
    {
      "Country": "Spain",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Jooicer.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Joinville.Se"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Madington.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Pr",
      "URL": "Brandmetrics.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Pr",
      "URL": "Vidispine.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Print",
      "URL": "Qwaya.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Juni.Co"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Bambuser.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Platforms & Carts",
      "URL": "Avensia.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Retail Proximity & Iot Marketing",
      "URL": "Extendaretail.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Retail Proximity & Iot Marketing",
      "URL": "Motiondisplay.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Attach.Io"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Refined.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Spreadsheetconverter.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Weld.Io"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Ariser.Se"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Brandsystems.Co…"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Crystallize.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Debroome.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Signifikant.Se"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Halon.Io"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Insertcoin.Se"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Ifragasatt.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Funnelmaker.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Jojka.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Speqta.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Optimization Personalization & Testing",
      "URL": "Sitegainer.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "123On.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Storykit.Io"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Weareimint.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Rationalbi.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Nepa.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Comintelli.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Data",
      "Landscape subcategory": "Customer Data Platform",
      "URL": "Datatalks.Se"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Data",
      "Landscape subcategory": "Dashboards & Data Visualization",
      "URL": "Dapresy.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Data",
      "Landscape subcategory": "Dashboards & Data Visualization",
      "URL": "Qlik.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Data",
      "Landscape subcategory": "Governance Compliance And Privacy",
      "URL": "Dporganizer.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Data",
      "Landscape subcategory": "Governance Compliance And Privacy",
      "URL": "Secureprivacy.Ai"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Data",
      "Landscape subcategory": "Governance Compliance And Privacy",
      "URL": "Konsento.Io"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Data",
      "Landscape subcategory": "Governance Compliance And Privacy",
      "URL": "Consentmanager.Net"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Cotunity.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Hoylu.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Management",
      "Landscape subcategory": "Projects & Workflow",
      "URL": "Favro.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Management",
      "Landscape subcategory": "Projects & Workflow",
      "URL": "Quinyx.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Management",
      "Landscape subcategory": "Talent Management",
      "URL": "Typelane.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Management",
      "Landscape subcategory": "Talent Management",
      "URL": "Muchskills.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Advocacy Loyalty & Referrals",
      "URL": "Awardit.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Advocacy Loyalty & Referrals",
      "URL": "Intermail.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Call Analytics & Management",
      "URL": "Remotely.Fm"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Community & Reviews",
      "URL": "Flattr.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Community & Reviews",
      "URL": "Hoodin.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Community & Reviews",
      "URL": "Panion.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Conversational Marketing And Chat",
      "URL": "Artificial-Solutions.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Occtoo.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Whereby.Com"
    },
    {
      "Country": "Sweden",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Influencers",
      "URL": "Tourn.Com"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Adhash.Org"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Flyerbee.Com"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Mobile Marketing",
      "URL": "Fusedeck.Com"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Print",
      "URL": "Socialease.Ch"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Search & Social Advertising",
      "URL": "Smartwall.Ai"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Affiliate Marketing & Management",
      "URL": "Lemonads.Com"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Gonnado.Com"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Kinetizine.Com"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Webnode.Com"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Teleport.Media"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Inside-Reality.Com"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Meloncast.Com"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Pimalion.Com"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Goaland.Ch"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Media-Impression.Com"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Online.Ch"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Filecamp.Com"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Friendly.Is"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Telxira.Com"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Rele.Ai"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "Swissmademarketing.Com"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Irewind.Com"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Data",
      "Landscape subcategory": "Audience/Marketing Data & Data Enhancement",
      "URL": "Geoctrl.Com"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Data",
      "Landscape subcategory": "Audience/Marketing Data & Data Enhancement",
      "URL": "1Plusx.Com"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "42Matters.Com"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Data",
      "Landscape subcategory": "Dashboards & Data Visualization",
      "URL": "Ecomap.Io"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Management",
      "Landscape subcategory": "Budgeting & Finance",
      "URL": "Sandpiper.Ch"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Beekeeper.Io"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Management",
      "Landscape subcategory": "Projects & Workflow",
      "URL": "Uhub.Io"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Call Analytics & Management",
      "URL": "Mitto.Ch"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Community & Reviews",
      "URL": "Reziew.Com"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Actricity.Com"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Novadoo.Com"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Sandsiv.Com"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Pulsesolutions.Com"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Zkipster.Com"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Indico.Com"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Eventboost.Com"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Live Chat & Chatbots",
      "URL": "Aiaibot.Com"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Live Chat & Chatbots",
      "URL": "Enterprisebot.Ai"
    },
    {
      "Country": "Switzerland",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Live Chat & Chatbots",
      "URL": "Masha.Ai"
    },
    {
      "Country": "Turkey",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Medialyzer.Com"
    },
    {
      "Country": "Turkey",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Reklamstore.Com"
    },
    {
      "Country": "Turkey",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Prisync.Com"
    },
    {
      "Country": "Turkey",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Replybutton.Com"
    },
    {
      "Country": "Turkey",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Smartmessage.Com"
    },
    {
      "Country": "Turkey",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "8Digits.Com"
    },
    {
      "Country": "Turkey",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Advermind.Com"
    },
    {
      "Country": "Turkey",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Relateddigital.Com"
    },
    {
      "Country": "Turkey",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Uplankton.Com"
    },
    {
      "Country": "Turkey",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Advocacy Loyalty & Referrals",
      "URL": "Hellosmpl.Com"
    },
    {
      "Country": "Turkey",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Dinamikcrm.Com"
    },
    {
      "Country": "Turkey",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Ofisim.Com"
    },
    {
      "Country": "Turkey",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Pisano.Co"
    },
    {
      "Country": "Turkey",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Alternacx.Com"
    },
    {
      "Country": "Turkey",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Sestek.Com"
    },
    {
      "Country": "Turkey",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Boomsonar.Com"
    },
    {
      "Country": "Turkey",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Socialpano.Com"
    },
    {
      "Country": "Ukraine",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Getprospect.Io"
    },
    {
      "Country": "Ukraine",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Logaster.Com"
    },
    {
      "Country": "Ukraine",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "K-Meta.Com"
    },
    {
      "Country": "Ukraine",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "Sitechecker.Pro"
    },
    {
      "Country": "Ukraine",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Espocrm.Com"
    },
    {
      "Country": "Ukraine",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Influencers",
      "URL": "Publicfast.Com"
    },
    {
      "Country": "Ukraine",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Youscan.Io"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Ve.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Propellerads.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Nws.Ai"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Flashtalking.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Exitbee.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Thisisdax.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Display & Programmatic Advertising",
      "URL": "Admixer.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Mobile Marketing",
      "URL": "Pmconnect.Co.Uk"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Mobile Marketing",
      "URL": "Jampp.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Mobile Marketing",
      "URL": "Adspruce.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Pr",
      "URL": "Releasewire.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Pr",
      "URL": "Answerthepublic.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Pr",
      "URL": "Ace.Media"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Print",
      "URL": "Scribeless.Co"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Print",
      "URL": "Trustads.Io"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Print",
      "URL": "Reachdesk.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Print",
      "URL": "Perceptaudit.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Print",
      "URL": "Hellomarket.Co.Uk"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Search & Social Advertising",
      "URL": "Seon.Io/"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Search & Social Advertising",
      "URL": "S-Branch.Co.Uk"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Advertising & Promotion",
      "Landscape subcategory": "Video Advertising",
      "URL": "Coull.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Affiliate Marketing & Management",
      "URL": "Securemlmsoftware.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Affiliate Marketing & Management",
      "URL": "Publisherdiscovery.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Affiliate Marketing & Management",
      "URL": "Affjet.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Affiliate Marketing & Management",
      "URL": "Trackonomics.Net"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Affiliate Marketing & Management",
      "URL": "Egass.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Channel Partner & Local Marketing",
      "URL": "Zeevou.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Channel Partner & Local Marketing",
      "URL": "Expandly.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Channel Partner & Local Marketing",
      "URL": "Chainsync.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Channel Partner & Local Marketing",
      "URL": "Brandmachine.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Channel Partner & Local Marketing",
      "URL": "Tune.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Freshrelevance.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Doofinder.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Blueprint.Store/"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Ecommercehq.Co.Uk"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Tobibots.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Recart.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Pureclarity.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Productcaster.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Pathfinder.Ie"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Getfirepush.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Cloudiq.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Bask.Yt"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Selro.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Marketing",
      "URL": "Blueprint.Store"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Platforms & Carts",
      "URL": "Plentymarkets.Eu"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Platforms & Carts",
      "URL": "Opencart.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Platforms & Carts",
      "URL": "Carts.Guru"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Platforms & Carts",
      "URL": "Advansys.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Platforms & Carts",
      "URL": "Veeqo.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Platforms & Carts",
      "URL": "Subscriptionflow.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Ecommerce Platforms & Carts",
      "URL": "Ksubaka.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Retail Proximity & Iot Marketing",
      "URL": "Bubbl.Tech"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Retail Proximity & Iot Marketing",
      "URL": "Brightpearl.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Retail Proximity & Iot Marketing",
      "URL": "Qminder.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Retail Proximity & Iot Marketing",
      "URL": "Shazam.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Retail Proximity & Iot Marketing",
      "URL": "Screen.Cloud"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Retail Proximity & Iot Marketing",
      "URL": "Edited.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Retail Proximity & Iot Marketing",
      "URL": "Zettle.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Retail Proximity & Iot Marketing",
      "URL": "Unicenta.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Retail Proximity & Iot Marketing",
      "URL": "Redant.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Commissionly.Io"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Touchapp.Co/"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Orgcharthub.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Ipresent.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Reply.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Betterproposals.Io"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Thoughtriver.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Exclaimer.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "E-Sign.Co.Uk"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Minelead.Io"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Commerce & Sales",
      "Landscape subcategory": "Sales Automation Enablement & Intelligence",
      "URL": "Snov.Io"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Zenar.Io"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Subhub.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Squiz.Net"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Sheety.Co"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Omcore.Net"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Asbrusoft.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Herothemes.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Teacommerce.Net"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Sheetsu.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Cms & Web Experience Management",
      "URL": "Madewithwagtail.Org"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Qurate.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Glorifyapp.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Storystream.Ai"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Vwriter.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Upscri.Be"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Showhows.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Us.Copify.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Beacon.By"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Bluelucy.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Content Marketing",
      "URL": "Messagecloud.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Col8.Net"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Campaignamp.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Desl.Net"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Epitomy.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Locksidesoftware.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Goods.Co.Uk"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Lobster-Uk.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Matrixcms.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Omio-Retail.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Onetimepim.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Oporteo.Co.Uk"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Pimberly.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Aspin.Co.Uk"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Redtechnology.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Truecommerce.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Mtivity.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Dam & Mrm & Pim",
      "URL": "Builtbybright.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Sendsmith.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Mailingmanager.Co.Uk"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Newzapp.Co.Uk"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Mailody.Io"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Kickdynamic.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Emailhippo.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Rocketseed.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Email Marketing",
      "URL": "Timetoreply.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Programmai.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Freeonlinesurveys.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Zuko.Io"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Textanywhere.Net"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Interfunnels.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Interactive Content",
      "URL": "Calculoid.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Zymplify.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Inspired.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Salestraction.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Salesseek.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Marketpowerpro.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Iceberg-Digital.Co.Uk"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Fuzeiq.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Dotdigital.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Dashcord.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Marketing Automation & Campaign/Lead Management",
      "URL": "Optinopoli.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Mobile Apps",
      "URL": "Nutshellapps.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Mobile Apps",
      "URL": "Voicesage.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Optimization Personalization & Testing",
      "URL": "Personifyxp.Tech/"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Optimization Personalization & Testing",
      "URL": "Webeo.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Optimization Personalization & Testing",
      "URL": "Traveltime.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Optimization Personalization & Testing",
      "URL": "Personifyxp.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Optimization Personalization & Testing",
      "URL": "Pagewiz.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Optimization Personalization & Testing",
      "URL": "Useinsider.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Optimization Personalization & Testing",
      "URL": "Convertize.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "Seotagg.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "Wordtracker.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "Varvy.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "Urlprofiler.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "Seranking.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "Ranktracker.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "Goingup.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "Fatjoe.Co"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Seo",
      "URL": "Raptor-Dmt.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Ipv.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Envsion.Io"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Vidello.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Piksel.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Dacast.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Vieworks.Io"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Eviid.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Content & Experience",
      "Landscape subcategory": "Video Marketing",
      "URL": "Synthesia.Io"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Audience/Marketing Data & Data Enhancement",
      "URL": "Loqate.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Audience/Marketing Data & Data Enhancement",
      "URL": "Datasift.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Audience/Marketing Data & Data Enhancement",
      "URL": "Intelligentpositioning.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Globalwebindex.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Alchemetrics-Uk.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Parkersoftware.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Lineup.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Bigsofatech.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Pxtech.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Segmentstream.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Prodsight.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Business/Customer Intelligence & Data Science",
      "URL": "Appraisal360.Co.Uk"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Dashboards & Data Visualization",
      "URL": "Autographicinsight.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Dashboards & Data Visualization",
      "URL": "Machinelabs.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Dashboards & Data Visualization",
      "URL": "Sharpcloud.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Dashboards & Data Visualization",
      "URL": "Avora.Io"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Dashboards & Data Visualization",
      "URL": "Vizlib.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Dashboards & Data Visualization",
      "URL": "Dodonaanalytics.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Dmp",
      "URL": "Dentsu.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Dmp",
      "URL": "Winpure.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Governance Compliance And Privacy",
      "URL": "Pimloc.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Governance Compliance And Privacy",
      "URL": "Trust-Hub.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Governance Compliance And Privacy",
      "URL": "Surecloud.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Governance Compliance And Privacy",
      "URL": "Onetrust.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Governance Compliance And Privacy",
      "URL": "Consentua.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Governance Compliance And Privacy",
      "URL": "This.Citizen.Is"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Ipaas Cloud/Data Integration & Tag Management",
      "URL": "Cyclr.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Ipaas Cloud/Data Integration & Tag Management",
      "URL": "Confluent.Io"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Ipaas Cloud/Data Integration & Tag Management",
      "URL": "Blueprism.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Ipaas Cloud/Data Integration & Tag Management",
      "URL": "Vastclicks.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Ipaas Cloud/Data Integration & Tag Management",
      "URL": "Robocloud.Co.Uk"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Ipaas Cloud/Data Integration & Tag Management",
      "URL": "Tyntec.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Ipaas Cloud/Data Integration & Tag Management",
      "URL": "Codelessplatforms.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Marketing Analytics Performance & Attribution",
      "URL": "Snowplowanalytics.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Marketing Analytics Performance & Attribution",
      "URL": "Prodlytic.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Mobile & Web Analytics",
      "URL": "Decibel.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Data",
      "Landscape subcategory": "Mobile & Web Analytics",
      "URL": "Popcornmetrics.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Agile & Lean Management",
      "URL": "Demand.Center"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Agile & Lean Management",
      "URL": "Agilitysystem.Net"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Budgeting & Finance",
      "URL": "Yoyowallet.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Cutover.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Twoodo.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Tessello.Co.Uk"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Crugo.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Clinked.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Ayoa.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Dragapp.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Simplifie.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Getriff.Co"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Collaboration",
      "URL": "Interact-Intranet.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Product Management",
      "URL": "Moteefe.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Product Management",
      "URL": "Bulbshare.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Product Management",
      "URL": "Upscope.Io"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Product Management",
      "URL": "Anymod.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Projects & Workflow",
      "URL": "Predictiveblack.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Projects & Workflow",
      "URL": "Workstack.Io"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Projects & Workflow",
      "URL": "Outplanr.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Projects & Workflow",
      "URL": "Screendragon.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Projects & Workflow",
      "URL": "Claromentis.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Projects & Workflow",
      "URL": "Bigchangeapps.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Projects & Workflow",
      "URL": "Intheloop.Io"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Talent Management",
      "URL": "Traktion.Ai"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Talent Management",
      "URL": "Willo.Video"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Talent Management",
      "URL": "Zokri.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Talent Management",
      "URL": "Targetinternet.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Talent Management",
      "URL": "Staffconnectapp.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Talent Management",
      "URL": "Kallidus.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Vendor Analysis",
      "URL": "Fsmartech.Co.Uk"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Vendor Analysis",
      "URL": "Stacker.App"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Vendor Analysis",
      "URL": "Marketingsoftwaremanager.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Vendor Analysis",
      "URL": "Productmood.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Vendor Analysis",
      "URL": "Martechbase.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Vendor Analysis",
      "URL": "Discovercrm.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Management",
      "Landscape subcategory": "Vendor Analysis",
      "URL": "Gatekeeperhq.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Advocacy Loyalty & Referrals",
      "URL": "Loyalty.Oappso.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Advocacy Loyalty & Referrals",
      "URL": "Incentivesmart.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Call Analytics & Management",
      "URL": "Threads.Cloud/"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Call Analytics & Management",
      "URL": "Infinity.Co"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Call Analytics & Management",
      "URL": "Phonexa.Uk"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Community & Reviews",
      "URL": "Voxpopme.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Community & Reviews",
      "URL": "Reviews.Co.Uk"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Community & Reviews",
      "URL": "Makerpad.Co"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Community & Reviews",
      "URL": "Feedbackexpress.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Community & Reviews",
      "URL": "Disciplemedia.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Community & Reviews",
      "URL": "Adoreboard.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Community & Reviews",
      "URL": "Reviews.Io"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Community & Reviews",
      "URL": "Endorsal.Io"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Rsoft.In"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Realtimecrm.Co.Uk"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Onepagecrm.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Junari.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Instream.Io"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Attio.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Getflg.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Apiscrm.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Format14Crm.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Bascrm.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Crm",
      "URL": "Easycrm.Me"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Odicci.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Imimobile.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Survey-Me.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Innertrends.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Inlinemanual.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Getenjoyhq.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Dunnhumby.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Customerthermometer.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Usefulfeedback.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Clicktools.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Chattermill.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Customer Experience Service & Success",
      "URL": "Stampede.Ai"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Iqpolls.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Integrate.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Crystalinteractive.Net"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Brighttalk.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Events Meetings & Webinars",
      "URL": "Heysummit.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Influencers",
      "URL": "Trail.Social"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Influencers",
      "URL": "Influencer.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Live Chat & Chatbots",
      "URL": "Tidio.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Vuyu.App"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Welikeit.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Sentiment.Io"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Publing.Co"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Orlo.Tech"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Missinglettr.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Lightful.Com"
    },
    {
      "Country": "United Kingdom",
      "Landscape category": "Social & Relationships",
      "Landscape subcategory": "Social Media Marketing & Monitoring",
      "URL": "Birdsonganalytics.Com"
    }
  ]