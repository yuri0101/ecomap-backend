const config = require("../config/config")

const mongoose = require('mongoose')
const ObjectId = mongoose.Types.ObjectId;

const StakeholderModel = require('../models/stakeholder.model')
const UserStakeholderModel = require('../models/user-team-stakeholder.model')
const IndustryModel = require('../models/industry.model')

async function main(){
    await mongoose.connect(config.mongoose.url, config.mongoose.options)
    let Stakeholders = await UserStakeholderModel.find({}).limit(10).populate('industries')
    for(let i = 0; i < Stakeholders.length; i++){
        console.log(Stakeholders[i])
    }
}

main()