const config = require("../config/config")

const mongoose = require('mongoose')
const ObjectId = mongoose.Types.ObjectId;
const request = require('request').defaults({ encoding: null });
const FileType = require('file-type');

const StakeholderModel = require('../models/stakeholder.model')
const UserStakeholderModel = require('../models/user-team-stakeholder.model')

const UploaderService = require("../services/uploader.service")
const ImageService = require("../services/image.service")

const CUTOFF_INDEX = 50

async function main(){
mongoose.connect(config.mongoose.url, config.mongoose.options).then(() => {

    let skeletonImage = "https://cdn.ecomap.io/core/ecomap-thumb.png"
    // request.get(skeletonImage, function (err, res, bufferBody) {
    //     ImageService.GenerateImageOptions(bufferBody).then(df=>{
    //         UploaderService.putObject({
    //             name:`ecomap-thumb.jpg`,
    //             data: df
    //         }).then(d=>{
    //             console.log(d)
    //         })
    //     })
    // });

    UserStakeholderModel.find({
        $and:[ 
            { images:{ $exists: false }},
            { info:{ $exists: true } }
        ]
    })
    .limit(100)
    .exec((err, stakeholders)=>{

        if(stakeholders.length == 0) return process.exit(0);

        for(let i=0; i < stakeholders.length; i++){
            let stakeholder = stakeholders[i]
            // console.log(`Stakeholder: ${stakeholder.id} Image: ${stakeholder.image}`)

            let stakeholderImage = stakeholder.image
            if(stakeholderImage && stakeholderImage.length > 5){
                request.get(stakeholderImage, function (err, res, bufferBody) {
                    if(!err){
                        ImageService.GenerateImageOptions(bufferBody).then(d=>{
                            let processedBuffer = d;
                            // FileType.fromBuffer(processedBuffer).then(dd=>{
                            //     console.log("File Type:",dd)
                            // })
                            UploaderService.putObject({
                                name:`${stakeholder.id}.jpg`,
                                data: processedBuffer
                            }).then(d=>{
                                //console.log(d)
                                let newLocation = d.Location.replace("eu-central-1.linodeobjects.com/","")
                                stakeholder.images = {
                                    thumb: newLocation,
                                    hq: stakeholder.image
                                }
                                stakeholder.save().then(d=>{
                                    console.log(`Stakeholder: ${stakeholder.id} Updated ${i} with ${newLocation}`)
                                    if(i == CUTOFF_INDEX) process.exit()
                                }).catch(e=>{
                                    console.log(`Error Updating: ${e}`)
                                })
                            }).catch(e=>{
                                console.error(e)
                            })
                        }).catch(e=>{

                            console.error(`Image Parsing Error: ${stakeholderImage} : ${e}`)
                            stakeholder.image = skeletonImage;

                            stakeholder.images = {
                                thumb: skeletonImage,
                                hq: null
                            }

                            stakeholder.meta = {
                                IMAGE_SOURCE:'skeleton',
                                SKELETON_REASON:'parsing-error'
                            }

                            stakeholder.save().then(d=>{
                                console.log(`Stakeholder: ${stakeholder.id} Updated ${i} with Skeleton`)
                                if(i == CUTOFF_INDEX) process.exit()
                            }).catch(e=>{
                                console.log(`Error Updating: ${e}`)
                            })

                        })
                    }else{
                        console.log("Image URL: ", stakeholderImage)
                        console.error(err)
                    }
                });
            }else{

                stakeholder.image = skeletonImage;

                stakeholder.images = {
                    thumb: skeletonImage,
                    hq: null
                }

                stakeholder.meta = {
                    IMAGE_SOURCE:'skeleton'
                }

                stakeholder.save().then(d=>{
                    console.log(`Stakeholder: ${stakeholder.id} Updated ${i} with Skeleton`)
                    if(i == CUTOFF_INDEX) process.exit()
                }).catch(e=>{
                    console.log(`Error Updating: ${e}`)
                })
            }
        }
        
    })
    
})
}

main()