const config = require("../../config/config")
const mongoose = require('mongoose')
const ObjectId = mongoose.Types.ObjectId;

const ClearBitModel = require("../../models/dw-clearbit.model")
const StakeholderModal = require("../../models/stakeholder.model")

const fetch = require('node-fetch')

async function getLogo(domain){
    let u = `https://www.googleapis.com/customsearch/v1?start=1&size=5&q=${domain}+logo&prettyPrint=false&searchType=image&cx=004902075774833659222:uiayohkb0hc&key=AIzaSyA763_gS1ZZhfQvfh0yNlOOqL4A1QOsCyw`
    let data = await fetch(u).then(r=>r.json())
    try{
        for(let i = 0; i < data.items.length; i++){
            let result = data.items[i];
            if(result.fileFormat == "image/jpeg"){
                return result.link
            }
        }
    }
    catch(e){
        return ''
    }
}

const generateHash = (title) => {
    let nt = title.toLowerCase().replace(/[\W_]+/g,"-")
    nt = nt.replace(/--+/g,"-")
    return `${nt}`
}

async function main(){
    let allModels = await ClearBitModel.find({
        meta_status:'PENDING'
    });
    for(let i = 68; i < allModels.length; i++){
        let m = allModels[i]
        let logo = await getLogo(m.domain)
        await StakeholderModal.create({
            slug: generateHash(m.name),
            name: m.name, 
            details: m.description,
            s_info:{
                website: m.domain,
            },
            info:{
                'Based in (mailing city)': m.meta['Based in (mailing city)']
            },  
            image: logo,
            meta_tags: ['SWISS_HEALTH']
        }).then(d=>{
            console.log("Stakeholder Added with Id: ", d._id)
        }).catch(e=>{
            console.log(`Error Creating Stakeholder: ${m.id}`, e.message)
        })
    }
}

mongoose.connect(config.mongoose.url, config.mongoose.options).then(() => {
    main().then(d=>{
        console.log("Executed ", d)
    })
})