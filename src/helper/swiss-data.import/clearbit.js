const config = require("../../config/config")
const data = require("./data")
const mongoose = require('mongoose')
const ObjectId = mongoose.Types.ObjectId;

const ClearBitModel = require("../../models/dw-clearbit.model")
const IndustryModel = require("../../models/industry.model")
let Clearbit = require("../../vendor/clearbit/")
var i = 0

function getDomain( url ){
    try{
        let u = new URL(url)
        let hn = u.hostname
        if(hn.indexOf("www.") != -1) hn = hn.replace("www.","")
        return hn
    }catch(e){
        let hn = url
        if(url.indexOf("www.") != -1) hn = url.replace('www.','')
        if(hn.indexOf("/") != -1) hn.replace(/\//ig,'')
        return hn
    }

}

async function main(domain, name, description, meta_info){
   
    //console.log(x, industries)
    let y = await ClearBitModel.findOne({domain})
    if(!y){

        //let x = await Clearbit.getInfo(domain)
        //x.clearbit_id = x.id;
        //delete x.id;
    
        return await ClearBitModel.create({   
            //...x, 
            domain,
            name,
            description,
            //cb_dump: x,
            meta:meta_info,
            meta_tag:'CUSTOM_SWISS_DIGITAL_HEALTH'
        }).then(d=>{
            let x = d ? d.id:"NONE:" + domain
            return x;
        }).catch(e=>{
            //console.log(x)
            if(e.code === 11000){
                throw new Error("Duplicate Key Error at: ", i, e.message)
            }else{
                console.error(`Error Creating ${i} Module for: ${domain}`, e)
                process.exit()
            }
        })

    }else{
        // let x = await Clearbit.getInfo(domain)
        // x.clearbit_id = x.id;
        // delete x.id;
        throw new Error(`Module Already There with ID as ${y._id}`)
    }
    
 
}

// var i = 0

//console.log(data[i]); process.exit;

mongoose.connect(config.mongoose.url, config.mongoose.options).then(() => {
    console.info('Connected to MongoDB');
    function updateData(){

        let m = data[i]

        let WebsiteDomain = getDomain(m.Website)

        main(
            WebsiteDomain, 
            m['Name'],
            m['Elevator Pitch'],
            {
                NAME: m['Name'],
                PITCH: m['Elevator Pitch'],
                WEBSITE: m['Website'],
                MAP_GROUP_TITLE: m['Einteilung Swiss Digital Health Map'],
                ADDRESS:{
                    TEXT: m['Address'],
                    ZIP_CODE: m['ZIP Code'],
                    CITY: m['Place'],
                    STATE: m['Canton'],
                },
                'Based in (mailing city)': m['Place']
            }
        ).then(e=>{
            console.log("Module Added : ", e)
        }).catch(e=>{
            console.error(`Error addding Module: ${e.message}`)
        })

        // IndustryModel.findOneOrCreate(
        //     {
        //         title:new RegExp(m['Landscape subcategory'], 'i')
        //     },
        //     {
        //         title: m['Landscape subcategory']
        //     },
        //     (err, subIndustry)=>{
                
        //         IndustryModel.findOneOrCreate(
        //             {
        //                 title: new RegExp(m['Landscape category'], 'i')
        //             },
        //             {
        //                 title: m['Landscape category']
        //             },
        //             (err, mainIndustry)=>{

        //                 if(mainIndustry.child.length == 0){
        //                     mainIndustry.child = [...mainIndustry.child, subIndustry._id]
        //                     mainIndustry.save((err, result)=>{
        //                         if(err) console.log(`Error Saving Industry: ${err}`)
        //                         else console.log("Industry Updated")
        //                     })
        //                 }

        //                 let domain = m['URL'].toLowerCase()

        //                 console.log("Industries: ", mainIndustry._id, subIndustry._id)
        //                 main(
        //                     domain, 
        //                     m['Country'],
        //                     [ObjectId(mainIndustry._id), ObjectId(subIndustry._id)]
        //                 ).then(e=>{
        //                     // console.log("Module Added : ", e)
        //                 })

        //             }
        //         )
                
        //     }
        // )

        setTimeout(()=>{
            i++;
            try{
                updateData()
            }
            catch(e){
                console.error("Error:", e)
                console.log("Done at Index: ", i);
                process.exit(0)
            }
        },100)

        //main(m.URL).then(e=>console.log(e))
    }
    updateData()
});

