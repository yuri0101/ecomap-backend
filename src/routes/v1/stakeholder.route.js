const express = require('express');
const app = express.Router();
const IndustryModel = require('../../models/industry.model');
const StakeholderModel = require('../../models/stakeholder.model');
const UserTeamModel = require('../../models/user-team.model');
const UserTeamStakeholderModel = require('../../models/user-team-stakeholder.model');

const esClient = require('../../vendor/elasticsearch');
const auth = require('../../middlewares/auth');
const { ObjectId } = require('mongoose').Types;
const { isValidObjectId } = require('mongoose');

const ping = require('web-pingjs');
const NodeCache = require('node-cache');
const myCache = new NodeCache();

const getDomain = (url) => {
  try {
    let u = new URL(url);
    let hn = u.hostname;
    if (hn.indexOf('www.') != -1) hn = hn.replace('www.', '');
    return hn;
  } catch (e) {
    let hn = url;
    if (url.indexOf('www.') != -1) hn = url.replace('www.', '');
    if (hn.indexOf('/') != -1) hn.replace(/\//gi, '');
    return hn;
  }
};

const validateWebsite = async (website) => {
  let domain = getDomain(website);

  try {
    let pingDomain = await ping(domain);
    console.log('PING_DOMAIN', pingDomain);
    return true;
  } catch (e) {
    let wwwDomain = `www.${domain}`;
    try {
      await ping(wwwDomain);
      return true;
    } catch (e) {
      return false;
    }
  }
};

const createUserTeam = async (userId) => {
  return await UserTeamModel.create({
    owner: ObjectId(userId),
  });
};

const LoadPrivateLibrary = async (userId, { page = 1, per_page = 50 }) => {
  if (userId) {
    let UserTeam = await UserTeamModel.findOne({
      owner: ObjectId(userId),
    });

    if (!UserTeam) UserTeam = await createUserTeam(userId);
    let UserLibrary = await UserTeamStakeholderModel.find(
      //{user_team: ObjectId(UserTeam.id)}
      {}
    )
      .sort({
        created_at: -1,
      })
      .skip(page ? (page - 1) * per_page : 0)
      .limit(per_page || 50);

    return UserLibrary;
  }
  return null;
};

const ElasticSearch = async (query) => {
  const response = await esClient.search({
    index: 'test',
    type: 'industries',
    body: {
      query: {
        bool: {
          filter: [
            {
              match_phrase: {
                title: query,
              },
            },
          ],
        },
      },
    },
  });
  return response;
};

app.get('/', auth(), async (req, res) => {
  let payload = req.query;
  let { q, page, mode } = payload;

  if (mode && mode == 'private') {
    const page_number = page ? page : 1;

    const LIB_CACHE_KEY = `${req.user.id}_private_library_page_${page_number}`;
    let privateLibraryCached = myCache.get(LIB_CACHE_KEY);

    if (!privateLibraryCached) {
      privateLibraryCached = await LoadPrivateLibrary(req.user ? req.user.id : null, {
        page: page_number,
        per_page: 50,
      });
      for (let i = 0; i < privateLibraryCached.length; i++) {
        privateLibraryCached[i].id = `_${privateLibraryCached[i].id}`;
      }
      myCache.set(LIB_CACHE_KEY, privateLibraryCached);
    }

    return res.json({
      success: true,
      status: 200,
      message: 'Loaded',
      data: {
        data: privateLibraryCached,
        meta: {
          pagination: {
            total: -1,
            next_page: true,
            count: 50,
            per_page: 50,
          },
        },
      },
    });
  }

  //
  // Disabled Elasticsearch for a while
  //
  // let ElasticResult = await ElasticSearch(q).catch(e=>{return {hits:{total:0}}})
  // if(ElasticResult && ElasticResult.hits.total > 50){
  //     return res.json({
  //         success: true,
  //         status: 200,
  //         message:'Loaded Search',
  //         data: {
  //             data:ElasticResult,
  //             meta:{
  //                 pagination:{
  //                     total: -1,
  //                     count: 50,
  //                     per_page: 50
  //                 }
  //             }
  //         }
  //     })
  // }

  const per_page = 50;
  let data = await StakeholderModel.find({
    name: {
      $regex: `.*${q}.*`,
    },
  })
    .sort({
      created_at: -1,
    })
    .skip(page ? (page - 1) * per_page : 0)
    .limit(per_page);

  for (let i = 0; i < data.length; i++) {
    data[i] = data[i].toJSON();
    data[i]['hash'] = data[i].id;
    data[i]['image'] = data[i]['images']['thumb'];
  }

  return res.json({
    success: true,
    status: 200,
    message: 'Loaded',
    data: {
      data,
      meta: {
        pagination: {
          total: -1,
          next_page: data.length < per_page ? false : true,
          count: 50,
          per_page: 50,
        },
      },
    },
  });
});

app.get('/info', async (req, res) => {
  let id = req.query.id;

  let Module = null;
  // Loading Private ID
  if (id.startsWith('_')) {
    let privateModuleId = id.replace('_', '');
    if (!id || !isValidObjectId(id)) return res.json({ success: false, error: 'Invalid Stakeholder Id' });
    Module = await UserTeamStakeholderModel.findById(privateModuleId).populate('industries');
  } else {
    if (!id || !isValidObjectId(id)) return res.json({ success: false, error: 'Invalid Stakeholder Id' });
    Module = await StakeholderModel.findById(id).populate('industries');
  }

  Module = Module.toJSON();

  Module.info = {
    ...Module.info,
    name: Module.name,
    details: Module.details || Module.s_info['details'] || '',
    website: Module.s_info['website'] || Module.info['website'] || '',
  };

  Module.connections = {
    industry: [],
    types: [],
  };

  Module.connections.industry = Module.industries;

  delete Module.industries;

  return res.json({
    success: true,
    error: '',
    data: Module,
  });
});

const generateHash = (title) => {
  let nt = title.toLowerCase().replace(/[\W_]+/g, '-');
  nt = nt.replace(/--+/g, '-');
  return `${nt}`;
};

app.post('/add', auth(), async (req, res) => {
  let payload = req.body;
  let { image, name, info } = payload;

  let website = info['website'];
  if (website && website.length > 1) {
    let isWebsiteValid = await validateWebsite(website);
    if (!isWebsiteValid)
      return res.json({
        success: false,
        code: 400,
        error: 'Website not valid',
        data: '',
      });
  }
  let StakeholderPayload = {
    name,
    slug: generateHash(name),
    details: info['details'] || '',
    s_info: {
      website: getDomain(website),
    },
    image,
    info: info,
    meta: {
      ADDED_BY: {
        USER: ObjectId(req.user.id),
      },
    },
  };

  try {
    await StakeholderModel.create(StakeholderPayload);
    return res.json({
      success: true,
      code: 200,
      error: 'Stakeholder Added',
      data: '',
    });
  } catch (e) {
    return res.json({
      success: false,
      code: 400,
      error: e.message,
      data: '',
    });
  }
});
app.post('/info/update', async (req, res) => {
  // Only Update Image for Now

  let payload = req.body;
  let sId = payload.id;
  let imageUrl = payload.info['image'] || '';
  let Stakeholder = await StakeholderModel.findOne({ _id: ObjectId(sId) });
  if (!Stakeholder)
    return res.json({
      success: false,
      error: 'Invalid Stakeholder',
    });

  Stakeholder.info['image'] = imageUrl;
  Stakeholder.image = imageUrl;

  await Stakeholder.save();
  return res.json({
    success: true,
    status: 200,
    error: '',
    data: 'Updated',
    message: 'Data Updated',
  });
});

app.post('/set-image', async (req, res) => {
  let payload = req.body;
  let sId = payload.id;
  let image = payload.image;

  let Stakeholder = await StakeholderModel.findOne({ _id: ObjectId(sId) });
  if (!Stakeholder)
    return res.json({
      success: false,
      error: 'Invalid Stakeholder',
    });

  Stakeholder.image = image;
  await Stakeholder.save();

  return res.json({
    success: true,
    status: 200,
    error: '',
    data: 'Data Updated',
    message: 'Data Updated',
  });
});

module.exports = app;
