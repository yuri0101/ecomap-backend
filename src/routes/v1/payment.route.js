const express = require('express');
const app = express.Router();

const auth = require('../../middlewares/auth');

const StripeHelper = require('../../vendor/stripe');

app.post('/subscribe', auth(), async (req, res) => {
  let payload = req.body;
  let { couponId, customerId, paymentId, paymentMethodId, priceId } = payload;

  try {
    let Coupon = await StripeHelper.GetCouponFromPromoCode(couponId);
    let UserSubscription = await StripeHelper.Subscribe({
      customer_id: customerId,
      payment_method_id: paymentMethodId,
      price_id: priceId,
      coupon_id: Coupon && Coupon.id ? Coupon.id : null,
    });

    // @todo: Upgrade User Plan

    return res.json({
      success: true,
      error: null,
      status: 200,
      data: UserSubscription,
    });
  } catch (e) {
    return res.json({
      success: false,
      error: e.message,
      status: 200,
      data: null,
    });
  }
});

app.post('/coupon', auth(), async (req, res) => {
  let couponCode = req.body.couponId;
  let Coupon = await StripeHelper.GetCouponFromPromoCode(couponCode);

  let Response = {
    success: true,
    error: '',
    status: 200,
    data: 'Invalid Coupon Code',
  };

  if (!Coupon) {
    Response.data = null;
    Response.error = 'Invalid Coupon Code';
    Response.success = false;
  }

  if (Coupon) {
    Response.success = true;
    Response.data = Coupon;
  }

  return res.json(Response);
});

app.post('/customer', auth(), async (req, res) => {
  let userId = req.user.id;

  let StripeCustomer = await StripeHelper.GetStripeCustomer({
    user_email: req.user.email,
  });

  return res.json({
    success: true,
    error: '',
    status: 200,
    data: StripeCustomer,
  });
});

app.get('/prices', async (req, res) => {
  let StripePrices = await StripeHelper.GetPrices();

  return res.json({
    success: true,
    error: '',
    status: 200,
    data: StripePrices,
  });
});

module.exports = app;
