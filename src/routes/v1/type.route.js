const express = require('express');
const app = express.Router();

app.all('/*', (req, res) => {
  res.json({
    success: true,
    error: '',
    status: 200,
    data: [
      { id: 1, name: 'Currency', details: null, connectors: ['is blockchain of', 'is hard fork of', 'is soft fork of'] },
      { id: 2, name: 'Exchange', details: null, connectors: ['is holding/trading'] },
      {
        id: 3,
        name: 'Mine',
        details: 'This stakeholders are mines that mine cryptocurrency',
        connectors: ['is owned by', 'is not run by'],
      },
      {
        id: 4,
        name: 'Organisation',
        details: null,
        connectors: ['is owning', 'in cooperation', 'in competition', 'is generating'],
      },
      {
        id: 7,
        name: 'Start easy',
        details: 'This library should contain a set of companies for anonymous users to create their first ecosystem',
        connectors: ['is owning'],
      },
      { id: 9, name: 'Brands', details: null, connectors: ['owned by'] },
      { id: 10, name: 'jang', details: 'gg', connectors: ['vev'] },
    ],
  });
});

module.exports = app;
