const express = require('express');
const app = express.Router();
const BoardModel = require('../../models/board.model');
const { ObjectId } = require('mongoose').Types;

const auth = require('../../middlewares/auth');

app.get('/delete', auth(), async (req, res) => {
  let userId = req.user.id;
  let BoardHash = req.query.hash;

  console.log(userId, BoardHash);

  let Board = await BoardModel.findOne({
    user: ObjectId(userId),
    hash: BoardHash,
    is_deleted: false,
  });

  if (!Board)
    return res.json({
      success: false,
      status: 400,
      error: 'No Such Board',
      data: 'No Such Board',
    });

  Board.is_deleted = true;
  await Board.save();

  return res.json({
    success: true,
    status: 200,
    error: 'Board Deleted',
    data: 'Board Deleted',
  });
});

app.get('/', auth(), async (req, res) => {
  let userId = req.user.id;
  let Boards = await BoardModel.find({
    user: ObjectId(userId),
    is_deleted: false,
  });

  let b = [];
  for (let i = 0; i < Boards.length; i++) {
    let board = Boards[i].toJSON();
    board = {
      ...board,
      boardId: board.id,
      public: board.status == 'PRIVATE' ? false : true,
      shape: board.settings.shape,
    };
    b.push(board);
  }

  return res.json({
    success: true,
    error: '',
    status: 200,
    data: b,
  });
});

app.get('/view', async (req, res) => {
  let hash = req.query.hash;
  let board = await BoardModel.findOne({
    hash,
  });

  console.log('BoardData:', hash, board);
  return res.json({
    success: true,
    error: '',
    status: 200,
    data: board,
  });
});
app.post('/store', auth(), async (req, res) => {
  let payload = req.body;
  let { modules, data, hash } = payload;

  // let Board = BoardModel.findOne({hash}).catch(e=>{console.error("Bord Error:",e)})
  // if(!Board) return res.json({success:false, error:'No Such Board'})

  let BoardPayload = {};

  BoardPayload.data = data;
  try {
    BoardPayload._data = { modules: JSON.parse(modules) };
  } catch (e) {
    console.log('No Modules Passed');
  }

  let Board = await BoardModel.findOneAndUpdate({ hash }, BoardPayload, {
    returnOriginal: false,
  });

  let b = Board.toJSON();
  return res.json({
    success: true,
    status: 200,
    data: {
      ...b,
      board: {
        ...b,
      },
    },
  });
});
app.post('/create', auth(), async (req, res) => {
  let payload = req.body;
  let { details, private, title } = payload;

  let Board = await BoardModel.create({
    user: req.user.id,
    title,
    details,
    status: private ? 'PRIVATE' : 'PUBLIC',
  });

  Board.hash = `${title.replace(/[^a-zA-Z]/g, '-').replace(/--+/g, '-')}-${Buffer.from(Board.id)
    .toString('base64')
    .replace(/=+/g, '')}`.toLowerCase();
  await Board.save();

  Board.boardId = Board.id;
  Board.public = Board.status == 'PRIVATE' ? false : true;
  Board.shape = Board.settings.shape;

  res.json({
    success: true,
    error: '',
    status: 200,
    data: Board,
    // data:{
    //     id:1,
    //     hash:'new-board',
    //     title:'',
    //     boardId:null,
    //     shape:'circle',
    //     public: true,
    //     versions:[]
    // }
  });
});

app.all('/*', (req, res) => {
  res.json({
    success: true,
    error: '',
    status: 200,
    data: [
      {
        id: 11,
        hash: 'd91978d2ca2398c211bbe088f5b6bf94',
        title: 'Blockchain Ecosystem 1',
        details: null,
        public: true,
        updated_at: '2021-03-22 19:58:47',
        created_at: '2018-11-04 15:59:23',
        versions: [],
      },
    ],
  });
});

module.exports = app;
