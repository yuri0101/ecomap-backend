const express = require('express');
const authRoute = require('./auth.route');
const userRoute = require('./user.route');
const typeRoute = require('./type.route');
const boardRoute = require('./board.route');
const paymentRoute = require('./payment.route');
const stakeholderRoute = require('./stakeholder.route');

const docsRoute = require('./docs.route');
const config = require('../../config/config');

const router = express.Router();

const defaultRoutes = [
  {
    path: '/auth',
    route: authRoute,
  },
  {
    path: '/user',
    route: userRoute,
  },
  {
    path: '/users',
    route: userRoute,
  },
  {
    path: '/stakeholders',
    route: stakeholderRoute,
  },
  {
    path: '/module',
    route: stakeholderRoute,
  },
  {
    path: '/types',
    route: typeRoute,
  },
  {
    path: '/board',
    route: boardRoute,
  },
  {
    path: '/boards',
    route: boardRoute,
  },
  {
    path: '/payment',
    route: paymentRoute,
  },
];

const devRoutes = [
  // routes available only in development mode
  {
    path: '/docs',
    route: docsRoute,
  },
];

defaultRoutes.forEach((route) => {
  router.use(route.path, route.route);
});

/* istanbul ignore next */
if (config.env === 'development') {
  devRoutes.forEach((route) => {
    router.use(route.path, route.route);
  });
}

module.exports = router;
