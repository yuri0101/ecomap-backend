const sharp = require('sharp')

const GenerateImageOptions = async (imageBuffer) => {
    return await sharp(imageBuffer).resize({
        fit: sharp.fit.contain,
        width: 100,
        height: 100
    })
    .jpeg({ quality: 90 })
    .toBuffer()
}

module.exports = {
    GenerateImageOptions
}