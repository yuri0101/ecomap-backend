const nodemailer = require('nodemailer');
const config = require('../config/config');
const logger = require('../config/logger');
const fs = require('fs')
const path = require("path");

const sesTransport = require('nodemailer-ses-transport');
const smtpPassword = require('aws-smtp-credentials');
const ejs = require('ejs');

const GET_FILE = (pathUrl) => {
  const data = fs.readFileSync(path.resolve(__dirname, pathUrl), 'utf8')
  return data
}

const EMAIL_TEMPLATES = {
  RESET_PASSWORD: (reset_link)=>{
    console.log("Reset Token:", reset_link)
    let ejsFile =  GET_FILE('../views/emails/reset-password.ejs')
    return ejs.render(ejsFile, {reset_link})
  }
}

// const transport = nodemailer.createTransport(config.email.smtp);
const transport = nodemailer.createTransport({
  service:'gmail',
  auth:{
    user:"dev.ecomap.io@gmail.com",
    pass:"tsjitabnpxialpuf"
  }
});

// const transport = nodemailer.createTransport({
//   port: 2587,
//   host: 'email-smtp.eu-central-1.amazonaws.com',
//   secure: true,
//   auth: {
//     user: 'AKIAQSUH432IHA7QKQNA',
//     pass: smtpPassword('BBf1Lb5FARcA3Ak0ejBU9rfMvZZ4xfsWLWI1YsZPeWNp'),
//   },
//   debug: true
// });

/* istanbul ignore next */
// if (config.env !== 'test') {
//   transport
//     .verify()
//     .then(() => logger.info('Connected to email server'))
//     .catch(() => logger.warn('Unable to connect to email server. Make sure you have configured the SMTP options in .env'));
// }

/**
 * Send an email
 * @param {string} to
 * @param {string} subject
 * @param {string} text
 * @returns {Promise}
 */
const sendEmail = async (to, subject, text) => {
  const msg = { from: config.email.from, to, subject, text };
  await transport.sendMail(msg);
};

/**
 * Send an Html email
 * @param {string} to
 * @param {string} subject
 * @param {string} html
 * @returns {Promise}
 */
const sendHtmlEmail = async (to, subject, html) => {
  const msg = { from: config.email.from, to, subject, html };
  await transport.sendMail(msg);
};

/**
 * Send reset password email
 * @param {string} to
 * @param {string} token
 * @returns {Promise}
 */
const sendResetPasswordEmail = async (to, token) => {
  const subject = 'Reset password';
  // replace this url with the link to the reset password page of your front-end app
  const resetPasswordUrl = `http://localhost:3000/reset/${token}`;
  const html = EMAIL_TEMPLATES.RESET_PASSWORD(resetPasswordUrl)
  console.log("Sending HTML", html)
  await sendHtmlEmail(to, subject, html);
};

module.exports = {
  transport,
  sendEmail,
  sendResetPasswordEmail,
};
