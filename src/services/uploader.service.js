var AWS = require('aws-sdk');

var s3 = new AWS.S3({
    accessKeyId: '1KYOE9R20JE0TALK6Z80' ,
    secretAccessKey: 'SiaWP9BNaVqMqD9PUDYt94kRoFm4IiQJQmJzsimF' ,
    endpoint: 'https://eu-central-1.linodeobjects.com' ,
    s3ForcePathStyle: true, // needed with minio?
    signatureVersion: 'v4',
    ACL:'public-read',
});




const putObject = (file) => {

    return new Promise((resolve, reject)=>{
        // Binary data base64
        const fileContent  = Buffer.from(file.data, 'binary');

        var timestamp = Math.floor(new Date().getTime() / 1000);
        var params = {
            Bucket: 'cdn.ecomap.io', 
            Key: `stakeholder/${file.name}`, 
            Body: fileContent,
            ACL:'public-read',
        };

        // Uploading files to the bucket
        s3.upload(params, function(err, data) {
            if (err) {
                reject(err);
            }
            resolve(data)
        });
    })
    
}

module.exports = {
    putObject
}