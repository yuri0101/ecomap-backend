const fetch = require('node-fetch')

const STRIPE_SECRET = process.env.STRIPE_SECRET
const StripeApi = require('stripe')(STRIPE_SECRET)



const StripeRequest = async(method, path, data = null) => {
    
    const BASE_URL = "https://api.stripe.com"

    let fetchOptions = {
        method: method == 'GET'?'GET':'POST',
        headers:{
            'authorization':`Bearer ${STRIPE_SECRET}`,
            'cache-control': 'no-cache',
            'content-type':'application/x-www-form-urlencoded'
        }
    }

    if(method == "POST"){
        fetchOptions.body = JSON.stringify(data)
    }

    return await fetch(`${BASE_URL}${path}`, fetchOptions).then(res=>res.json()).catch(e=>{console.error(e)})
}

const Stripe = {
    GetPrices: async () => {
        const STRIPE_PRODUCT_ID = process.env.STRIPE_PRODUCT_ID || 'prod_HwptWo4DM52jXh';
        let data = await StripeRequest('GET', '/v1/prices', {product: STRIPE_PRODUCT_ID})
        console.log("Stripe Product ID", STRIPE_PRODUCT_ID)
        return data
    },
    GetStripeCustomer: async ({
        user_email,
        stripe_customer_id = null
    }) => {
        if(stripe_customer_id){

            let StripeCustomer = await StripeRequest('GET',`/v1/customers/${stripe_customer_id}`)
            return StripeCustomer

        }else{

            let StripeCustomer = await StripeApi.customers.create({
                email: user_email
            })

            console.log("StripeCustomer:",StripeCustomer)
            return StripeCustomer
            
        }
    },
    GetCouponFromPromoCode: async (promoCode) => {
        let AllPromoCodes = await StripeRequest('GET','/v1/promotion_codes');
        let PromoCode = AllPromoCodes.data.filter(e=> e.code == promoCode.toUpperCase());
        //console.log("PromoCode:", PromoCode)
        if(PromoCode.length > 0) return PromoCode[0].coupon;
        return false
    },
    ValidateCoupon: async(couponId) => {
        let coupon = await StripeRequest('POST',`/v1/coupons/${couponId}`)
        return coupon
    },
    AddPaymentMethodToCustomer: async({
        customer_id, 
        payment_method_id
    }) =>{

        let r = await StripeRequest('POST',`/v1/payment_methods/${payment_method_id}/attach`)
        return r

    },
    Subscribe: async({
        customer_id, 
        payment_method_id, 
        price_id,
        coupon_id = null
    }) => {

        await StripeApi.paymentMethods.attach(
            payment_method_id,
            {customer: customer_id}
        );
        // await this.AddPaymentMethodToCustomer({customer_id, payment_method_id})

        let Payload = {
            customer: customer_id,
            default_payment_method: payment_method_id,
            items: [
                {
                    price: price_id
                }
            ]
        }

        if(coupon_id) Payload.coupon = coupon_id

        let SubscriptionResponse = await StripeApi.subscriptions.create(Payload);
        return SubscriptionResponse
    }
}

module.exports = Stripe;
