const fetch = require('node-fetch')

const getInfo = async (domain) => {
    let base_url = "https://company.clearbit.com/v2/companies/find?domain="
    return await fetch(base_url + domain,{
        headers:{
            'Authorization':'Basic c2tfOGEwZjI2OTBlN2NmZjA0ZWMxNmJiOWY3MmQxY2E4NDU6'
        }
    }).then(res=>res.json())        
}

module.exports = {
    getInfo
}