const elasticsearch = require('elasticsearch');
const esClient = new elasticsearch.Client({
  host: 'localhost:9200',
  log: 'trace',
  apiVersion: '5.6', // use the same version of your Elasticsearch instance
});

module.exports = esClient