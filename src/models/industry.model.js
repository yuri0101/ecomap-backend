const mongoose = require('mongoose');
const { toJSON } = require('./plugins');
const { tokenTypes } = require('../config/tokens');

const industrySchema = mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      index: true,
      unique: true,
    },
    details: {
      type: String,
    },
    child: [{ type: mongoose.Schema.Types.ObjectId, ref: 'industries' }],
  },
  {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
  }
);

// add plugin that converts mongoose to json
industrySchema.plugin(toJSON);

industrySchema.statics.findOneOrCreate = function findOneOrCreate(condition, data, callback) {
  const self = this;
  self.findOne(condition, (err, result) => {
    return result
      ? callback(err, result)
      : self.create(data, (err, result) => {
          return callback(err, result);
        });
  });
};

/**
 * @typedef Industry
 */
const Industry = mongoose.model('industries', industrySchema);

module.exports = Industry;
