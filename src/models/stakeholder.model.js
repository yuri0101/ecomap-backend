const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const { toJSON, paginate } = require('./plugins');
const { roles } = require('../config/roles');

const stakeholderSchema = mongoose.Schema(
  {
    old_id: {
      type: Number,
      unique: false,
    },
    slug: {
      type: String,
      unique: true,
      index: true,
      required: true,
    },
    name: {
      type: String,
      required: true,
      index: true,
    },
    details: {
      type: String,
    },
    s_info: {
      website: {
        type: String,
        index: true,
      },
    },
    info: {
      type: mongoose.Schema.Types.Mixed,
      required: true,
    },
    metrics: {
      employee: {
        record_date: { type: Date },
        min: { type: Number },
        max: { type: Number },
        avg: { type: Number },
      },
      revenue: {
        record_date: { type: Date },
        min: { type: Number },
        max: { type: Number },
        avg: { type: Number },
      },
    },
    image: {
      type: String,
      default: '',
    },
    images: {
      thumb: {
        type: String,
        default: '',
      },
      hq: {
        type: String,
        default: '',
      },
    },
    stype: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'StakeholderType',
    },
    tags: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Tag',
      },
    ],
    industries: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'industries',
      },
    ],
    connections: [
      {
        relationship: {
          type: String,
          enum: ['PARENT_COMPANY', 'ACQUISITION', 'SUBSIDIARY'],
        },
        stakeholder: { type: mongoose.Schema.Types.ObjectId, ref: 'stakeholders' },
      },
    ],
    revisions: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'StakeholderRevision',
      },
    ],
    meta: {
      type: mongoose.Schema.Types.Mixed,
    },
    meta_info: {
      type: mongoose.Schema.Types.Mixed,
    },
    meta_tags: [
      {
        type: String,
      },
    ],
  },
  {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
  }
);

// add plugin that converts mongoose to json
stakeholderSchema.plugin(toJSON);
stakeholderSchema.plugin(paginate);

stakeholderSchema.post('save', function (error, doc, next) {
  if (error.name === 'MongoError' && error.code === 11000) {
    next(new Error(`Stakeholder with name: ${doc.name} already exists`));
  } else {
    next(error);
  }
});

/**
 * @typedef Stakeholder
 */
const Stakeholder = mongoose.model('Stakeholder', stakeholderSchema);

module.exports = Stakeholder;
