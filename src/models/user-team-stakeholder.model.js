const mongoose = require('mongoose');
const { toJSON } = require('./plugins');
const { tokenTypes } = require('../config/tokens');

const userTeamStakeholder = mongoose.Schema(
  {
    user_team: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'UserTeam',
      required: true,
    },
    stakeholder: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Stakeholder',
      default: null,
    },
    name: {
      type: String,
      required: true,
    },
    details: {
      type: String,
    },
    image: {
      type: String,
      default: '',
    },
    images: {
      thumb: {
        type: String,
        default: '',
      },
      hq: {
        type: String,
        default: '',
      },
    },
    s_info: {
      website: {
        type: String,
        unique: true,
        sparse: true,
      },
      country: {
        type: String,
      },
    },
    info: {
      type: mongoose.Schema.Types.Mixed,
      required: true,
    },
    metrics: {
      employee: {
        record_date: { type: Date },
        min: { type: Number },
        max: { type: Number },
        avg: { type: Number },
        range: { type: String },
        exact: { type: Number },
      },
      revenue: {
        record_date: { type: Date },
        min: { type: Number },
        max: { type: Number },
        avg: { type: Number },
        range: { type: String },
        exact: { type: Number },
      },
    },
    tags: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'tags',
      },
    ],
    industries: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'industries',
      },
    ],
    connections: [
      {
        relationship: {
          type: String,
          enum: ['PARENT_COMPANY', 'ACQUISITION', 'SUBSIDIARY'],
        },
        stakeholder: { type: mongoose.Schema.Types.ObjectId, ref: 'stakeholders' },
      },
    ],
    meta: {
      type: mongoose.Schema.Types.Mixed,
    },
    meta_info: {
      type: mongoose.Schema.Types.Mixed,
    },
    meta_tags: [
      {
        type: String,
      },
    ],
    _private: {
      data_source: {
        clearbit: {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'DW_ClearBit',
          unique: true,
          sparse: true, // Unique if not null
        },
      },
    },
  },
  {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
  }
);

// add plugin that converts mongoose to json
userTeamStakeholder.plugin(toJSON);

/**
 * @typedef UserTeamStakeholder
 */
const UserTeamStakeholder = mongoose.model('UserTeamStakeholder', userTeamStakeholder);

module.exports = UserTeamStakeholder;
