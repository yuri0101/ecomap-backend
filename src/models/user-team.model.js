const mongoose = require('mongoose');
const { toJSON } = require('./plugins');
const { tokenTypes } = require('../config/tokens');

const userTeamSchema = mongoose.Schema(
  {
    owner: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'User',
      required: true,
      unique: true, // As of now 1 user can have 1 team only
    },
    title: {
      type: String,
      default: 'Personal Library',
    },
  },
  {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
  }
);

// add plugin that converts mongoose to json
userTeamSchema.plugin(toJSON);

/**
 * @typedef UserTeam
 */
const UserTeam = mongoose.model('UserTeam', userTeamSchema);

module.exports = UserTeam;
