const mongoose = require('mongoose');
const { toJSON } = require('./plugins');

const stakeholderTypeSchema = mongoose.Schema(
  {
    old_id: { type: Number },
    name: { type: String, required: true },
    details: { type: String },
    connectors: [{ type: String }],
  },
  {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
  }
);

// add plugin that converts mongoose to json
stakeholderTypeSchema.plugin(toJSON);

/**
 * @typedef StakeholderType
 */
const StakeholderType = mongoose.model('StakeholderType', stakeholderTypeSchema);

module.exports = StakeholderType;
