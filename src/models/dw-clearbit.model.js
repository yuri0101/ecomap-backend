const mongoose = require('mongoose');
const { toJSON } = require('./plugins');

const dwClearbitSchema = mongoose.Schema(
  {
    clearbit_id: {
      type: String,
    },
    name: {
      type: String,
      required: true,
    },
    legalName: String,
    domain: {
      type: String,
      required: true,
      index: true,
      unique: true,
    },
    domainAliases: [{ type: String }],
    site: mongoose.Schema.Types.Mixed,
    category: {
      sector: String,
      industryGroup: String,
      industry: String,
      subIndustry: String,
      sicCode: String,
      naicsCode: String,
    },
    tags: [
      {
        type: String,
      },
    ],
    description: String,
    foundedYear: Number,
    location: String,
    timeZone: String,
    utcOffset: String,
    geo: mongoose.Schema.Types.Mixed,
    logo: String,
    facebook: mongoose.Schema.Types.Mixed,
    linkedin: mongoose.Schema.Types.Mixed,
    twitter: mongoose.Schema.Types.Mixed,
    crunchbase: mongoose.Schema.Types.Mixed,
    emailProvider: Boolean,
    type: String,
    ticker: String,
    identifiers: mongoose.Schema.Types.Mixed,
    phone: String,
    metrics: mongoose.Schema.Types.Mixed,
    indexedAt: String,
    tech: [{ type: String }],
    techCategories: [{ type: String }],
    parent: mongoose.Schema.Types.Mixed,
    ultimateParent: mongoose.Schema.Types.Mixed,
    country: {
      type: String,
      index: true,
    },
    meta: {
      type: mongoose.Schema.Types.Mixed,
    },
    cb_dump: {
      type: mongoose.Schema.Types.Mixed,
    },
    meta_tag: {
      // Used for Identification of a particular Import // Prefix with CUSTOM_ for Manual Imports
      type: String,
      index: true,
    },
    meta_status: {
      type: String,
      enum: ['PENDING', 'IN_SYNC'],
      default: 'PENDING',
      index: true,
    },
    industries: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'industries',
      },
    ],
  },
  {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
  }
);

// add plugin that converts mongoose to json
dwClearbitSchema.plugin(toJSON);

/**
 * @typedef dwClearbit
 */
const dwClearbit = mongoose.model('DW_ClearBit', dwClearbitSchema);

module.exports = dwClearbit;
