const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');

const boardSchema = mongoose.Schema(
  {
    platform_version: {
      type: String,
      enum: ['V1', 'V2'],
      default: 'V2',
    },
    hash: {
      type: String,
      index: true,
    },
    user: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'User',
      required: true,
    },
    title: {
      type: String,
      required: true,
    },
    details: {
      type: String,
    },
    // New Version
    data: {
      type: mongoose.Schema.Types.Mixed,
    },
    // Old Version
    _data: {
      configs: {
        type: mongoose.Schema.Types.Mixed,
      },
      modules: [
        {
          type: mongoose.Schema.Types.Mixed,
        },
      ],
      markers: [
        {
          type: mongoose.Schema.Types.Mixed,
        },
      ],
      groups: {
        type: mongoose.Schema.Types.Mixed,
      },
      links: [
        {
          type: mongoose.Schema.Types.Mixed,
        },
      ],
    },
    settings: {
      shape: {
        type: String,
        enum: ['square', 'circle'],
        default: 'circle',
      },
    },
    status: {
      type: String,
      enum: ['PUBLIC', 'PRIVATE'],
      default: 'PRIVATE',
    },
    is_archived: {
      type: Boolean,
      default: false,
    },
    is_deleted: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
  }
);

// add plugin that converts mongoose to json
boardSchema.plugin(toJSON);
boardSchema.plugin(paginate);

/**
 * @typedef User
 */
const User = mongoose.model('Board', boardSchema);

module.exports = User;
