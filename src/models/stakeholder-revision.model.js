const mongoose = require('mongoose');
const { toJSON } = require('./plugins');

const stakeholderRevisionSchema = mongoose.Schema(
  {
    status: {
      type: String,
      enum: ['PENDING', 'APPROVED', 'REJECTED'],
      default: 'PENDING',
    },
    stakeholder: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Stakeholder',
      required: true,
    },
    revision_type: {
      type: String,
      enum: ['INTERNAL', 'EXTERNAL'],
      default: 'EXTERNAL',
      required: true,
    },
    user_id: { type: String },
    data_source: {
      type: String,
      enum: ['CLEARBIT', 'ECOMAP_USER'],
      required: true,
    },
    info: {
      type: mongoose.Schema.Types.Mixed,
      required: true,
    },
    metrics: {
      type: mongoose.Schema.Types.Mixed,
      required: true,
    },
  },
  {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
  }
);

// add plugin that converts mongoose to json
stakeholderRevisionSchema.plugin(toJSON);

/**
 * @typedef StakeholderRevision
 */
const StakeholderRevision = mongoose.model('StakeholderRevision', stakeholderRevisionSchema);

module.exports = StakeholderRevision;
